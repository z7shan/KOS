// **** general options - testing

#define TESTING_ENABLE_ASSERTIONS    1
#define TESTING_ENABLE_STATISTICS    1
#define TESTING_ENABLE_DEBUGGING     1
#define TESTING_ENABLE_VP_SPIN       1
//#define TESTING_ALWAYS_MIGRATE       1
#define TESTING_DISABLE_HEAP_CACHE   1 // KOS only
//#define TESTING_PTHREAD_SPINLOCK     1 // libfibre only

// **** general options - safer execution

//#define TESTING_DISABLE_LOADBALANCER 1
//#define TESTING_DISABLE_ALLOC_LAZY   1 // KOS only
//#define TESTING_DISABLE_DEEP_IDLE    1 // KOS only
//#define TESTING_DISABLE_PREEMPTION   1 // KOS only
//#define TESTING_REPORT_INTERRUPTS    1 // KOS only
//#define TESTING_MUTEX_ERRORCHECKING  1 // libfibre only

// **** general options - alternative design

#define TESTING_LOCKFREE_READYQUEUE  1
//#define TESTING_BLOCKING_LOCKFREE    1

// **** libfibre options - event handling design

#define TESTING_MASTER_POLLER        1 // have master poller regardless of poller fibre
//#define TESTING_POLLER_FIBRES        1 // vs. per-scheduler poller pthread
#define TESTING_POLLER_IDLEWAIT      1 // vs. wake up poller anytime
#define TESTING_POLLER_IDLETIMEDWAIT 1 // vs. wait indefinitely for idle
#define TESTING_POLLSYNC_OPTIMISTIC  1 // sync vs. no sync before I/O
#define TESTING_BULK_RESUME          1 // vs. individual resume
#define TESTING_EXPONENTIAL_EPOLL    1 // vs. flat same-size epoll calls

// **** KOS console/serial output configuration

//#define TESTING_DEBUG_STDOUT         1
#define TESTING_STDOUT_DEBUG         1
#define TESTING_STDERR_DEBUG         1

// **** KOS-specific tests

//#define TESTING_KEYCODE_LOOP         1
//#define TESTING_LOCK_TEST            1
#define TESTING_TCP_TEST             1
#define TESTING_MEMORY_HOG           1
#define TESTING_PING_LOOP            1
//#define TESTING_TIMER_TEST           1
