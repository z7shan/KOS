/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _IntrusiveContainer_h_
#define _IntrusiveContainer_h_ 1

#include "generic/basics.h"

// NOTE WELL: The simple design using 'next' and 'prev' typed as link
// pointers (instead of T*) and downcasting only works, if Link is the first
// class that T inherits from.

template<typename T,size_t CNT=1> class SingleLink;
template<typename T,size_t CNT=1> class DoubleLink;

template<typename T,size_t NUM=0,size_t CNT=1,typename LT=SingleLink<T,CNT>> class IntrusiveStack;
template<typename T,size_t NUM=0,size_t CNT=1,typename LT=SingleLink<T,CNT>> class IntrusiveQueue;
template<typename T,size_t NUM=0,size_t CNT=1,typename LT=DoubleLink<T,CNT>> class IntrusiveRing;
template<typename T,size_t NUM=0,size_t CNT=1,typename LT=DoubleLink<T,CNT>> class IntrusiveList;
template<typename T,size_t NUM=0,size_t CNT=1,typename LT=SingleLink<T,CNT>,bool Blocking=false> class IntrusiveQueueMPSC;

template<typename T,size_t CNT> class SingleLink {
  template<typename,size_t,size_t,typename> friend class IntrusiveStack;
  template<typename,size_t,size_t,typename> friend class IntrusiveQueue;
  template<typename,size_t,size_t,typename,bool> friend class IntrusiveQueueMPSC;
  struct {
    SingleLink* next;
  } link[CNT];
protected:
  SingleLink() {
#if TESTING_ENABLE_ASSERTIONS
    for (size_t i = 0; i < CNT; i++) { link[i].next = nullptr; }
#endif
  }
} __packed;

template<typename T,size_t CNT> class DoubleLink {
  template<typename,size_t,size_t,typename> friend class IntrusiveStack;
  template<typename,size_t,size_t,typename> friend class IntrusiveQueue;
  template<typename,size_t,size_t,typename> friend class IntrusiveRing;
  template<typename,size_t,size_t,typename> friend class IntrusiveList;
  template<typename,size_t,size_t,typename,bool> friend class IntrusiveQueueMPSC;
  struct {
    DoubleLink* next;
    DoubleLink* prev;
  } link[CNT];
protected:
  DoubleLink() {
#if TESTING_ENABLE_ASSERTIONS
    for (size_t i = 0; i < CNT; i++) { link[i].next = link[i].prev = nullptr; }
#endif
  }
} __packed;

template<typename T, size_t NUM, size_t CNT, typename LT> class IntrusiveStack {
  static_assert(NUM < CNT, "NUM >= CNT");
public:
  typedef LT Link;

private:
  Link* head;

public:
  IntrusiveStack() : head(nullptr) {}
  bool empty() const { return head == nullptr; }

  T*              front()       { return head; }
  const T*        front() const { return head; }

  static T*       next(      Link& elem) { return (      T*)elem.link[NUM].next; }
  static const T* next(const Link& elem) { return (const T*)elem.link[NUM].next; }

  static bool     test(const Link& elem) { return elem.link[NUM].next; }

  static void     clear(Link& elem) {
#if TESTING_ENABLE_ASSERTIONS
    elem.link[NUM].next = nullptr;
#endif
  }

  void push(Link& first, Link& last) {
    GENASSERT1(!test(last), FmtHex(&first));
    last.link[NUM].next = head;
    head = first;
  }

  void push(Link& elem) {
    push(elem, elem);
  }

  T* pop() {
    GENASSERT1(!empty(), FmtHex(this));
    Link* last = head;
    head = last->link[NUM].next;
    clear(*last);
    return (T*)last;
  }

  T* pop(size_t& count) { // returns pointer to last element popped
    GENASSERT1(!empty(), FmtHex(this));
    Link* last = head;
    for (size_t i = 1; i < count; i += 1) {
      if slowpath(last->link[NUM].next == nullptr) count = i; // breaks loop and sets count
      else last = last->link[NUM].next;
    }
    head = last->link[NUM].next;
    clear(*last);
    return (T*)last;
  }

  void transferFrom(IntrusiveStack& es, size_t& count) {
    if (es.empty()) return;
    Link* first = es.front();
    Link* last = es.pop(count);
    push(*first, *last);
  }
} __packed;


template<typename T, size_t NUM, size_t CNT, typename LT> class IntrusiveQueue {
  static_assert(NUM < CNT, "NUM >= CNT");
public:
  typedef LT Link;

private:
  Link* head;
  Link* tail;

public:
  IntrusiveQueue() : head(nullptr), tail(nullptr) {}
  bool empty() const {
    GENASSERT1((head == nullptr) == (tail == nullptr), FmtHex(this));
    return head == nullptr;
  }

  T*              front()       { return       (T*)head; }
  const T*        front() const { return (const T*)head; }
  T*              back()        { return       (T*)tail; }
  const T*        back()  const { return (const T*)tail; }

  static T*       next(      Link& elem) { return (      T*)elem.link[NUM].next; }
  static const T* next(const Link& elem) { return (const T*)elem.link[NUM].next; }

  static bool     test(const Link& elem) { return elem.link[NUM].next; }

  static void     clear(Link& elem) {
#if TESTING_ENABLE_ASSERTIONS
    elem.link[NUM].next = nullptr;
#endif
  }

  void push(Link& first, Link& last) {
    GENASSERT1(!test(last), FmtHex(&first));
    if (!head) head = &first;
    else {
      GENASSERT1(tail != nullptr, FmtHex(this));
      tail->link[NUM].next = &first;
    }
    tail = &last;
  }

  void push(Link& elem) {
    push(elem, elem);
  }

  T* pop() {
    GENASSERT1(!empty(), FmtHex(this));
    Link* last = head;
    head = last->link[NUM].next;
    if (tail == last) tail = nullptr;
    clear(*last);
    return (T*)last;
  }

  T* pop(size_t& count) {
    GENASSERT1(!empty(), FmtHex(this));
    Link* last = head;
    for (size_t i = 1; i < count; i += 1) {
      if slowpath(last->link[NUM].next == nullptr) count = i; // breaks loop and sets count
      else last = last->link[NUM].next;
    }
    head = last->link[NUM].next;
    if (tail == last) tail = nullptr;
    clear(*last);
    return (T*)last;
  }

  T* popAll() {
    GENASSERT1(!empty(), FmtHex(this));
    Link* last = tail;
    head = tail = nullptr;
    clear(*last);
    return (T*)last;
  }

  void transferFrom(IntrusiveQueue& eq, size_t& count) {
    if (eq.empty()) return;
    Link* first = eq.front();
    Link* last = eq.pop(count);
    push(*first, *last);
  }

  void transferAllFrom(IntrusiveQueue& eq) {
    if (eq.empty()) return;
    Link* first = eq.front();
    Link* last = eq.popAll();
    push(*first, *last);
  }
} __packed;

template<typename T, size_t NUM, size_t CNT, typename LT> class IntrusiveRing {
  static_assert(NUM < CNT, "NUM >= CNT");
public:
  typedef LT Link;

public:
  static T*       next(      Link& elem) { return       (T*)elem.link[NUM].next; }
  static const T* next(const Link& elem) { return (const T*)elem.link[NUM].next; }
  static T*       prev(      Link& elem) { return       (T*)elem.link[NUM].prev; }
  static const T* prev(const Link& elem) { return (const T*)elem.link[NUM].prev; }

  static bool     test(const Link& elem) { return elem.link[NUM].next && elem.link[NUM].prev; }

  static void     clear(Link& first, Link& last) {
#if TESTING_ENABLE_ASSERTIONS
    first.link[NUM].prev = last.link[NUM].next = nullptr;
#endif
  }

  static void init(Link& elem) {
    elem.link[NUM].prev = elem.link[NUM].next = &elem;
  }

  static void insert_before(Link& next, Link& first, Link&last) {
    GENASSERT1(first.link[NUM].prev == nullptr, FmtHex(&first));
    GENASSERT1(last.link[NUM].next == nullptr, FmtHex(&last));
    GENASSERT1(test(next), FmtHex(&next));
    next.link[NUM].prev->link[NUM].next = &first;
    first.link[NUM].prev = next.link[NUM].prev;
    next.link[NUM].prev = &last;
    last.link[NUM].next = &next;
  }

  static void insert_before(Link& next, Link& elem) {
    insert_before(next, elem, elem);
  }

  static void insert_after(Link& prev, Link& first, Link& last) {
    GENASSERT1(first.link[NUM].prev == nullptr, FmtHex(&first));
    GENASSERT1(last.link[NUM].next == nullptr, FmtHex(&last));
    GENASSERT1(test(prev), FmtHex(&prev));
    prev.link[NUM].next->link[NUM].prev = &last;
    last.link[NUM].next = prev.link[NUM].next;
    prev.link[NUM].next = &first;
    first.link[NUM].prev = &prev;
  }

  static void insert_after(Link& prev, Link& elem) {
    insert_after(prev, elem, elem);
  }

  static T* remove(Link& first, Link& last) {
    GENASSERT1(test(first), FmtHex(&first));
    GENASSERT1(test(last), FmtHex(&last));
    first.link[NUM].prev->link[NUM].next =  last.link[NUM].next;
     last.link[NUM].next->link[NUM].prev = first.link[NUM].prev;
    clear(first, last);
    return (T*)&last;
  }

  static T* remove(Link& elem) {
    return remove(elem, elem);
  }
} __packed;

template<typename T, size_t NUM, size_t CNT, typename LT> class IntrusiveList : public IntrusiveRing<T,NUM,CNT,LT> {
public:
  typedef LT Link;

private:
  Link anchor;

public:
  IntrusiveList()  { anchor.link[NUM].next = anchor.link[NUM].prev = &anchor; }
  ~IntrusiveList() { anchor.link[NUM].next = anchor.link[NUM].prev = nullptr; }

  using IntrusiveRing<T,NUM,CNT,LT>::next;
  using IntrusiveRing<T,NUM,CNT,LT>::prev;
  using IntrusiveRing<T,NUM,CNT,LT>::clear;
  using IntrusiveRing<T,NUM,CNT,LT>::insert_before;
  using IntrusiveRing<T,NUM,CNT,LT>::insert_after;
  using IntrusiveRing<T,NUM,CNT,LT>::remove;

  T*              front()       { return next(anchor); }
  const T*        front() const { return next(anchor); }
  T*              back()        { return prev(anchor); }
  const T*        back()  const { return prev(anchor); }

  T*              edge()        { return       (T*)&anchor; }
  const T*        edge()  const { return (const T*)&anchor; }

  bool            empty() const { return front() == edge(); }

  T* remove(Link& first, size_t& count) {
    GENASSERT1(test(first), FmtHex(&first));
    T* last = &first;
    for (size_t i = 1; i < count; i += 1) {
      if slowpath(last->link[NUM].next == edge()) count = i; // breaks loop and sets count
      else last = last->link[NUM].next;
    }
    return remove(first, *last);
  }

  T* removeAll() { return remove(*front(), *back()); }

  void push_front(Link& elem)               { insert_before(*front(), elem); }
  void push_back(Link& elem)                { insert_after (*back(),  elem); }
  void splice_back(Link& first, Link& last) { insert_after (*back(),  first, last); }

  T* pop_front() { GENASSERT1(!empty(), FmtHex(this)); return remove(*front()); }
  T* pop_back()  { GENASSERT1(!empty(), FmtHex(this)); return remove(*back()); }

  void transferFrom(IntrusiveList& el, size_t& count) {
    if (el.empty()) return;
    Link* first = el.front();
    Link* last = el.remove(*first, count);
    splice_back(*first, *last);
  }

  void transferAllFrom(IntrusiveList& el) {
    if (el.empty()) return;
    Link* first = el.front();
    Link* last = el.removeAll();
    splice_back(*first, *last);
  }
} __packed;

// inspired by
// http://www.1024cores.net/home/lock-free-algorithms/queues/intrusive-mpsc-node-based-queue
// https://github.com/samanbarghi/MPSCQ/blob/master/src/MPSCQueue.h
template<typename T, size_t NUM, size_t CNT, typename LT, bool Blocking> class IntrusiveQueueMPSC {
  static_assert(NUM < CNT, "NUM >= CNT");
public:
  typedef LT Link;

private:
  Link  stub;
  Link* head;
  Link* tail;

  // peek/pop operate in chunks of elements and re-append stub after each chunk
  // after re-appending stub, tail points to stub, if no further insertions -> empty!
  bool checkStub() {
    if (head == &stub) {                                    // if current front chunk is empty
      if (Blocking) {                                       // BLOCKING:
        Link* expected = &stub;                             //   check if tail also points at stub -> empty?
        Link* xchg = (Link*)(uintptr_t(expected) | 1);      //   if yes, mark queue empty
        bool empty = __atomic_compare_exchange_n(&tail, &expected, xchg, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
        if (empty) return false;                            //   queue is empty and is marked now
        if (uintptr_t(expected) & 1) return false;          //   queue is empty and was marked before
      } else {                                              // NONBLOCKING:
        if (tail == &stub) return false;                    //   check if tail also points at stub -> empty?
      }
      while (!(Link* volatile)stub.link[NUM].next) Pause(); // producer in append
      head = (Link* volatile)stub.link[NUM].next;           // remove stub
      push(stub);                                           // re-append stub at end
    }
    return true;
  }

public:
  IntrusiveQueueMPSC(): head(&stub), tail(&stub) {
    if (Blocking) tail = (Link*)(uintptr_t(tail) | 1);      // mark queue empty
  }

  bool push(Link& first, Link& last) {
    last.link[NUM].next = nullptr;
    Link* prev = __atomic_exchange_n(&tail, &last, __ATOMIC_RELAXED); // swing tail to last of new element(s)
    bool empty = false;
    if (Blocking) {                                         // BLOCKING:
      empty = uintptr_t(prev) & 1;                          //   check empty marking
      prev = (Link*)(uintptr_t(prev) & ~uintptr_t(1));      //   clear marking
    }
    prev->link[NUM].next = &first;                          // append segments to previous tail
    return empty;
  }

  bool push(Link& elem) { return push(elem, elem); }

  T* peek() {
    if (!checkStub()) return nullptr;
    return (T*)head;
  }

  template<bool Peeked = false>
  T* pop() {
    if (!Peeked && !checkStub()) return nullptr;
    T* retval = (T*)head;                                   // head will be returned
    while (!(Link* volatile)head->link[NUM].next) Pause();  // producer in append
    head = (Link* volatile)head->link[NUM].next;            // remove head
#if TESTING_ENABLE_ASSERTIONS
    retval->link[NUM].next = nullptr;                       // satisfy consistency checks
#endif
    return retval;
  }

  bool transferAllFrom(IntrusiveQueue<T,NUM,CNT,LT>& eq) {
    if (eq.empty()) return false;
    Link* first = eq.front();
    Link* last = eq.popAll();
    return push(*first, *last);
  }
};

#endif /* _IntrusiveContainer_h_ */
