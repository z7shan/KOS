/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _SpinLocks_h_
#define _SpinLocks_h_ 1

#include "generic/basics.h"

template<typename T>
static inline bool _CAS(T *ptr, T expected, T desired, int success_memorder = __ATOMIC_SEQ_CST, int failure_memorder = __ATOMIC_RELAXED) {
  T* exp = &expected;
  return __atomic_compare_exchange_n(ptr, exp, desired, false, success_memorder, failure_memorder);
}

// pro: simple
// con: unfair, cache contention
template<size_t SpinStart = 1, size_t SpinEnd = 1024>
class BinaryLock {
protected:
  volatile bool locked;
public:
  BinaryLock() : locked(false) {}
  bool test() { return locked; }
  bool tryAcquire() {
    if (locked) return false;
    return !__atomic_test_and_set(&locked, __ATOMIC_SEQ_CST);
  }
  void acquire() {
    size_t spin = SpinStart;
    for (;;) {
      if fastpath(!__atomic_test_and_set(&locked, __ATOMIC_SEQ_CST)) break;
      for (size_t i = 0; i < spin; i += 1) Pause();
      if (spin < SpinEnd) spin += spin;
      while (locked) Pause();
    }
  }
  void release() {
    GENASSERT(locked);
    __atomic_clear(&locked, __ATOMIC_SEQ_CST);
  }
} __caligned;

// pro: simple, owner locking
// con: unfair, cache contention
template<size_t SpinStart = 1, size_t SpinEnd = 1024, typename T = mword, T noOwner=limit<mword>()>
class BinaryOwnerLock {
  volatile T owner;
  mword counter;
public:
  BinaryOwnerLock() : owner(noOwner), counter(0) {}
  bool test() { return owner != noOwner; }
  mword tryAcquire(T caller) {
    if (owner != caller) {
      if (owner != noOwner) return 0;
      if slowpath(!_CAS((T*)&owner, noOwner, caller)) return 0;
    }
    counter += 1;
    return counter;
  }
  mword acquire(T caller) {
    if (owner != caller) {
      size_t spin = SpinStart;
      for (;;) {
        if fastpath(_CAS((T*)&owner, noOwner, caller)) break;
        for (size_t i = 0; i < spin; i += 1) Pause();
        if (spin < SpinEnd) spin += spin;
        while (owner != noOwner) Pause();
      }
    }
    counter += 1;
    return counter;
  }
  template<bool full = false>
  mword release(T caller) {
    GENASSERT(owner == caller);
    counter = full ? 0 : (counter - 1);
    if (counter == 0) __atomic_store_n(&owner, noOwner, __ATOMIC_SEQ_CST);
    return counter;
  }
} __caligned;

// pro: fair
// con: cache contention, cache line bouncing
class TicketLock {
  volatile mword serving;
  mword ticket;
public:
  TicketLock() : serving(0), ticket(0) {}
  bool test() { return serving != ticket; }
  bool tryAcquire() {
    if (serving != ticket) return false;
    mword tryticket = serving;
    return _CAS(&ticket, tryticket, tryticket + 1);
  }
  void acquire() {
    mword myticket = __atomic_fetch_add(&ticket, 1, __ATOMIC_SEQ_CST);
    while (myticket != serving) Pause();
  }
  void release() {
    GENASSERT(sword(ticket-serving) > 0);
    __atomic_fetch_add(&serving, 1, __ATOMIC_SEQ_CST);
  }
} __caligned;

// pro: no cache contention -> scalability
// con: storage node, lock bouncing -> use cohorting?
// tested acquire/release memory ordering -> failure?
class MCSLock {
public:
  struct Node {
    Node* volatile next;
    volatile bool wait;
  };
private:
  Node* tail;
public:
  MCSLock() : tail(nullptr) {}
  bool test() { return tail != nullptr; }
  void acquire(Node& n) {
    n.next = nullptr;
    Node* prev = __atomic_exchange_n(&tail, &n, __ATOMIC_SEQ_CST);
    if (!prev) return;
    n.wait = true;   // store order guaranteed with TSO
    prev->next = &n; // store order guaranteed with TSO
    while (n.wait) Pause();
  }
  void release(Node& n) {
    GENASSERT(tail != nullptr);
    if (!n.next) {
      if (_CAS(&tail, &n, (Node*)nullptr)) return;
      while (!n.next) Pause();
    }
    n.next->wait = false;
  }
} __caligned;

template<typename Lock>
class SpinLockRW {
  Lock           wlock;
  volatile bool  writer;
  volatile mword readers __caligned;
public:
  SpinLockRW() : writer(false), readers(0) {}
  void acquireRead() {
    for (;;) {
      __atomic_add_fetch( &readers, 1, __ATOMIC_SEQ_CST);
      if (!wlock.test()) break;
      __atomic_sub_fetch( &readers, 1, __ATOMIC_RELAXED);
      while (wlock.test()) Pause();
    }
    writer = false;
  }
  void acquireWrite() {
    wlock.acquire();
    while (readers) Pause();
    writer = true;
  }
  bool tryAcquireRead() {
    __atomic_add_fetch( &readers, 1, __ATOMIC_SEQ_CST);
    if (wlock.test()) {
      __atomic_sub_fetch( &readers, 1, __ATOMIC_RELAXED);
      return false;
    }
    writer = false;
    return true;
  }
  bool tryAcquireWrite() {
    if (readers) return false;
    if (!wlock.tryAcquire()) return false;
    if (readers) {
      wlock.release();
      return false;
    }
    writer = true;
    return true;
  }
  void release() {
    if (writer) wlock.release();
    else __atomic_sub_fetch( &readers, 1, __ATOMIC_SEQ_CST);
  }
};

#endif /* _SpinLocks_h_ */
