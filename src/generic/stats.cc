/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "generic/basics.h"
#include "generic/stats.h"

#if TESTING_ENABLE_STATISTICS

bool StatsObject::print(ostream& os) {
  os << name << ' ' << FmtHex(obj);
  return true;
}

void StatsObject::printAll(ostream& os) {
  for (StatsObject* o : *lst) {
    if (o->print(os)) os << std::endl;
    delete o;
  }
}

bool ProcessorStats::print(ostream& os) {
  if (enq == 0) return false;
  StatsObject::print(os);
  os << ' ' << enq << ' ' << bulk << ' ' << deq << ' ' << steal << ' ' << idle << ' ' << wake;
  return true;
}

bool SchedulerStats::print(ostream& os) {
  if (share + rotate + steal == 0) return false;
  StatsObject::print(os);
  os << ' ' << share << ' ' << rotate << ' ' << steal;
  return true;
}

bool PollerStats::print(ostream& os) {
  if (events == 0) return false;
  StatsObject::print(os);
  os << ' ' << events;
  return true;
}

#endif /* TESTING_ENABLE_STATISTICS */
