/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _KernelProcessor_h_
#define _KernelProcessor_h_ 1

#include "extern/mtrand/mtrand.h"
#include "generic/IntrusiveContainers.h"
#include "runtime/BaseProcessor.h"
#include "machine/HardwareProcessor.h"

class Thread;
class Machine;

struct AddressSpaceMarker : public IntrusiveList<AddressSpaceMarker>::Link {
  volatile sword enterEpoch;
};

class KernelProcessor : public HardwareProcessor, public BaseProcessor {
  friend class KernelAddressSpace; // kernASM
  friend class AddressSpace;       // userASM
  AddressSpaceMarker kernASM;
  AddressSpaceMarker userASM;
  MTRand_int32 rng;

  KernelLock    haltLock;
  volatile bool haltFlag;
  volatile bool preemptFlag;

  inline void halt(bool);
  inline void idleLoop(bool irqs) __noreturn;
  static void idleLoopSetup(KernelProcessor*, bool irqs) __noreturn;

public:
  KernelProcessor() : HardwareProcessor(this), haltFlag(true), preemptFlag(true) {}
  void setScheduler(Scheduler& sched, _friend<Machine>) { BaseProcessor::setScheduler(sched); }
  void start(funcvoid0_t func, bool irqs) __noreturn;

  void seedRNG(mword s) { rng.seed(s); }
  mword random() { return rng(); }

  bool preemptable() {
    ScopedLock<KernelLock> al(haltLock);
    return preemptFlag;
  }

  void wakeProcessor() {
    ScopedLock<KernelLock> al(haltLock);
    haltFlag = false;
  }

  virtual void wakeUp() {
    DBG::outl(DBG::Idle, "send WakeIPI to ", getIndex());
    stats->wake.count();
    sendWakeIPI();
  }
};

static inline KernelProcessor& CurrProcessor() {
  KernelProcessor* p = LocalProcessor::self();
  GENASSERT(p);
  return *p;
}

static inline Scheduler& CurrScheduler() {
  return CurrProcessor().getScheduler();
}

#endif /* _KernelProcessor_h_ */
