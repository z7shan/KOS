/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _StackContext_h_
#define _StackContext_h_ 1

#include "generic/IntrusiveContainers.h" 
#include "runtime/Runtime.h"
#include "runtime/Stack.h"

#include <map>

static const mword  topPriority = 0;
static const mword  defPriority = 1;
static const mword  maxPriority = 2;
static const mword idlePriority = 3;

class VirtualProcessor;
class Scheduler;
class ResumeUnit;
class EventEngine;

#if TESTING_ENABLE_DEBUGGING
typedef IntrusiveList<StackContext,1,2,DoubleLink<StackContext,2>> GlobalStackList;
extern SystemLock*      _globalStackLock;
extern GlobalStackList* _globalStackList;
#define STACKLINKCOUNT 2
#else
#define STACKLINKCOUNT 1
#endif

typedef IntrusiveList<StackContext,0,STACKLINKCOUNT,DoubleLink<StackContext,STACKLINKCOUNT>> StackList;
typedef IntrusiveQueue<StackContext,0,STACKLINKCOUNT,DoubleLink<StackContext,STACKLINKCOUNT>> StackQueue;
#if TESTING_BLOCKING_LOCKFREE
typedef IntrusiveQueueMPSC<StackContext,0,STACKLINKCOUNT,DoubleLink<StackContext,STACKLINKCOUNT>,true> StackMPSC;
#else
typedef IntrusiveQueueMPSC<StackContext,0,STACKLINKCOUNT,DoubleLink<StackContext,STACKLINKCOUNT>> StackMPSC;
#endif

class StackContext : public StackList::Link {
  vaddr          stackPointer;  // holds stack pointer while stack inactive
  VirtualProcessor* currProcessor; // current resume processor
  VirtualProcessor* nextProcessor; // next processor for migration
  bool           affinity;      // affinity to processor (no load balancing)
  mword          priority;      // scheduling priority

  enum State { Running, Prepared, Suspended } state;
  ResumeUnit* resumeUnit;       // race: unblock vs. timeout

  StackContext(const StackContext&) = delete;
  const StackContext& operator=(const StackContext&) = delete;

  // central stack switching routine
  enum SwitchCode { Yield = 'Y', Migrate = 'M', Suspend = 'S', Terminate = 'T' };
  template<SwitchCode> inline bool switchStack();

  // these routines are called immediately after the stack switch
  static void postYield    (StackContext* prevStack);
  static void postMigrate  (StackContext* prevStack);
  static void postSuspend  (StackContext* prevStack);
  static void postTerminate(StackContext* prevStack);

  // resumption internal interfaces
  void resumeInternalPrep();
  void resumeInternal();

  // internal migration interface, only used for set Affinity
  inline void setMigrationTarget(VirtualProcessor&);

  // change processor
  inline void changeProcessor(VirtualProcessor&);

  // internal constructor only used for delegation by other constructors
  StackContext(VirtualProcessor&, bool affinity = false);
protected:
  // constructor/destructors can only be called by derived classes
  StackContext();
  StackContext(Scheduler&, bool bg = false);
  ~StackContext() { destructor(); }
  void destructor();

  void initStackPointer(vaddr sp) {
    stackPointer = align_down(sp, stackAlignment);
  }

public:
  // direct switch to new stack
  void direct(ptr_t func, _friend<SystemProcessor>) __noreturn {
    stackDirect(stackPointer, func, nullptr, nullptr, nullptr, nullptr);
  }

  // set up new stack
  void setup(ptr_t func, ptr_t p1 = nullptr, ptr_t p2 = nullptr, ptr_t p3 = nullptr, ptr_t p4 = nullptr) {
    stackPointer = stackInit(stackPointer, func, p1, p2, p3, p4);
  }

  // set up new stack and resume for concurrent execution
  void start(ptr_t func, ptr_t p1 = nullptr, ptr_t p2 = nullptr, ptr_t p3 = nullptr, ptr_t p4 = nullptr) {
    setup(func, p1, p2, p3, p4);
    resumeInternal();
  }

  // context switching interfaces; apply to CurrStack()
  static bool yield();
  static bool preempt();
  static void terminate() __noreturn;
  void suspend(_friend<ResumeUnit>);   // non-static; restricted to ResumeUnit

  // Running -> Prepared; Prepared -> Suspended is attempted in postSuspend()
  void prepareSuspend(_friend<ResumeUnit>) {
    GENASSERT1(state == Running, FmtHex(this));
    __atomic_store_n( &state, Prepared, __ATOMIC_SEQ_CST );
  }

  // Prepared/Suspended -> Running; resume stack later, if necessary
  template<bool ResumeNow=true>
  VirtualProcessor* resume() {
    State prevState = __atomic_exchange_n( &state, Running, __ATOMIC_SEQ_CST );
    GENASSERT1(prevState != Running, FmtHex(this));
    if (prevState != Suspended) return nullptr;
    if (ResumeNow) resumeInternal();
    else resumeInternalPrep();
    return currProcessor;
  }

  // set ResumeUnit with resumption information
  void setResumeUnit(ResumeUnit& ru, _friend<ResumeUnit>) {
    GENASSERT1(state == Prepared, FmtHex(this));
    __atomic_store_n( &resumeUnit, &ru, __ATOMIC_RELAXED );
  }

  // race between different possible resumers -> winner cancels the others
  ResumeUnit* checkResumeUnit() {
    return __atomic_exchange_n( &resumeUnit, nullptr, __ATOMIC_RELAXED );
  }

  // affinity, priority
  StackContext* setAffinity(bool a = true) { affinity = a; return this; }
  bool getAffinity() const                 { return affinity; }
  StackContext* setPriority(mword p)       { priority = p; return this; }
  mword getPriority() const                { return priority; }

  // set affinity & migrate to specific processor
  StackContext* setAffinity(VirtualProcessor&);

  // migrate to scheduler without affinity
  StackContext* migrate(Scheduler&);     // can be called from other stack
  static void   migrateSelf(Scheduler&); // operates on current stack

  // special direct migration needed for libfibre's disk I/O
  static VirtualProcessor& migrateSelf(Scheduler&, _friend<EventEngine>);
  static void              migrateSelf(VirtualProcessor&, _friend<EventEngine>);
};

struct ResumeQueue {
  size_t count;
#if TESTING_LOCKFREE_READYQUEUE && TESTING_BLOCKING_LOCKFREE
  StackQueue queue;
  void push(StackContext& sc) { queue.push(sc); count += 1; }
#else
  StackQueue queue[maxPriority];
  void push(StackContext& sc) { queue[sc.getPriority()].push(sc); count += 1; }
#endif
  ResumeQueue() : count(0) {}
};

typedef std::map<VirtualProcessor*,ResumeQueue> ProcessorResumeSet;

#endif /* _StackContext_h_ */
