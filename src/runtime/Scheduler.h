/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Scheduler_h_
#define _Scheduler_h_ 1

#include "runtime/BaseProcessor.h"

class Scheduler {
  typedef BinaryLock<> RingLock;
  RingLock        ringLock;
  BaseProcessor*  ringProc;
  volatile size_t ringCount;

  VirtualProcessor bgProc;

  SchedulerStats*  stats;

protected:
  SystemLock      idleLock;
  ProcessorList   idleList;
  volatile size_t idleCount;

  BaseProcessor* findIdleProcessorLocked() {
    if (idleList.empty()) return nullptr;
    BaseProcessor* proc = idleList.front();
    proc->idleList(_friend<Scheduler>()) = false;
    idleList.remove(*proc);
    idleCount -= 1;
    Runtime::debugS("Processor ", FmtHex(proc), " off idle list");
    return proc;
  }

public:
  Scheduler() : ringProc(nullptr), ringCount(0), bgProc(this), idleCount(0) {
    stats = new SchedulerStats(this);
  }

  size_t procCount() const { return ringCount; }

  void insert(BaseProcessor& proc) {
    ScopedLock<RingLock> sl(ringLock);
    if (ringProc == nullptr) {
      ProcessorRing::init(proc);
      ringProc = &proc;
    } else {
      ProcessorRing::insert_after(*ringProc, proc);
    }
    ringCount += 1;
  }

  void remove(BaseProcessor& proc) {
    ScopedLock<RingLock> sl(ringLock);
    GENASSERT(ringProc);
    // move ringProc, if necessary
    if (ringProc == &proc) ringProc = ProcessorRing::next(*ringProc);
    // ring empty?
    if (ringProc == &proc) ringProc = nullptr;
    ProcessorRing::remove(proc);
    ringCount -= 1;
  }

  VirtualProcessor& placement(_friend<StackContext> fs, bool bg = false) {
    if (bg) return bgProc;
    ScopedLock<RingLock> sl(ringLock);
    GENASSERT(ringProc);
    ringProc = ProcessorRing::next(*ringProc);
    return *ringProc;
  }

  VirtualProcessor& loadBalance(VirtualProcessor& proc, _friend<StackContext> fs) {
    if (idleCount && idleLock.tryAcquire()) {
      VirtualProcessor* idler = findIdleProcessorLocked();
      idleLock.release();
      if (idler) {
        stats->share.count();
        return *idler;
      }
    }
    stats->rotate.count();
    return placement(fs);
  }

  StackContext* schedule(BaseProcessor& proc, _friend<StackContext> fs) {
    StackContext* sc = proc.dequeue(_friend<Scheduler>());
    if (sc) return sc;
    sc = bgProc.dequeueSafe(_friend<Scheduler>());
    if (sc) return sc;
    return proc.idle(_friend<Scheduler>());
  }

  size_t addIdleProcessor(BaseProcessor& proc) {
    ScopedLock<SystemLock> sl(idleLock);
    if (bgProc.load()) return 0;
    proc.idleList(_friend<Scheduler>()) = true;
    idleList.push_back(proc);
    idleCount += 1;
    Runtime::debugS("Processor ", FmtHex(&proc), " on idle list");
    return idleCount;
  }

  void removeIdleProcessor(BaseProcessor& proc) {
    ScopedLock<SystemLock> sl(idleLock);
    if (proc.idleList(_friend<Scheduler>())) {
      idleList.remove(proc);
      idleCount -= 1;
      Runtime::debugS("Processor ", FmtHex(&proc), " off idle list");
    }
  }

  BaseProcessor* findIdleProcessor() {
    ScopedLock<SystemLock> sl(idleLock);
    return findIdleProcessorLocked();
  }
};

#endif /* _Scheduler_h_ */
