/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _BlockingSync_h_
#define _BlockingSync_h_ 1

#include "runtime/StackContext.h"

#include <list>
#include <map>
#include <set>

// RULE: can acquire BlockingQueue lock after Timer lock, but not vice versa!
template<typename Lock>
class TimerQueueGeneric {
  friend class TimeoutUnit;
  template<typename,bool> friend class TimeoutSynchronizationUnit;
  Lock lock;
  std::multimap<Time,StackContext*> queue;
public:
  typedef typename std::multimap<Time,StackContext*>::iterator iterator;
  iterator insert(const Time& timeout, StackContext* sc) {
    lock.acquire();
    Runtime::debugB( "Stack ", FmtHex(sc), " set timeout ", timeout);
    iterator ret = queue.insert( {timeout, sc} ); // set up timeout
    if (timeout <= queue.begin()->first) Runtime::notifyTimeout(timeout);
    return ret;
  }
  inline void sleep(const Time& timeout);
  inline void checkExpiry(const Time& now);
};

typedef SystemLock TimerQueueLock;
typedef TimerQueueGeneric<TimerQueueLock> TimerQueue;
extern TimerQueue* defaultTimerQueue;

class ResumeUnit {
  void unlock() {}
  template<typename Lock, typename... Args>
  void unlock(Lock& bl, Args&... ol) {
    bl.release();
    unlock(ol...);
  }
protected:
  template<typename... Args>
  void unlockAndSuspend(StackContext& ct, bool resumeRace, Args&... ol) {
    GENASSERTN(&ct == CurrStack(), FmtHex(&ct), " != ", FmtHex(CurrStack()));
    ct.prepareSuspend(_friend<ResumeUnit>());
    if (resumeRace) ct.setResumeUnit(*this, _friend<ResumeUnit>());
    Runtime::DisablePreemption(); // unlock would otherwise enable preemption
    unlock(ol...);
    ct.suspend(_friend<ResumeUnit>());
    Runtime::EnablePreemption();  // enable preemption now
  }
public:
  virtual void cancelTimeout() {}
  virtual void cancelSync(StackContext* ot = nullptr) {}
};

class TimeoutUnit : public virtual ResumeUnit {
protected:
  TimerQueue& tQueue;
  TimerQueue::iterator titer;
  void prepare(StackContext& ct, const Time& timeout) {
    titer = tQueue.insert(timeout, &ct);
  }
public:
  TimeoutUnit(TimerQueue& tq) : tQueue(tq) {}
  void suspend(const Time& timeout, StackContext& ct = *CurrStack()) {
    prepare(ct, timeout);
    unlockAndSuspend(ct, true, tQueue.lock);
  }
  virtual void cancelTimeout() {
    ScopedLock<TimerQueueLock> al(tQueue.lock);
    tQueue.queue.erase(titer);
  }
};

template<typename Lock, bool withQueue>
class SynchronizationUnit : public virtual ResumeUnit {
protected:
  Lock& bLock;
  bool timedOut;
  void prepare() {
    static_assert(withQueue == false, "wrong 'suspend' without queue");
    GENASSERT(bLock.test());
  }
  void prepare(StackContext& ct, StackList& queue) {
    static_assert(withQueue == true, "wrong 'suspend' with queue");
    GENASSERT(bLock.test());
    queue.push_back(ct);
  }
public:
  SynchronizationUnit(Lock& bl) : bLock(bl), timedOut(false) {}
  bool suspend(bool resumeRace = true, StackContext& ct = *CurrStack()) {
    prepare();
    unlockAndSuspend(ct, resumeRace, bLock);
    return !timedOut;
  }
  bool suspend(StackList& queue, StackContext& ct = *CurrStack()) {
    prepare(ct, queue);
    unlockAndSuspend(ct, true, bLock);
    return !timedOut;
  }
  virtual void cancelSync(StackContext* ot) {
    timedOut = true;
    if (!ot) return;
    ScopedLock<Lock> al(bLock);
    StackList::remove(*ot);
  }
};

template<typename Lock, bool withQueue>
class TimeoutSynchronizationUnit : public TimeoutUnit, public SynchronizationUnit<Lock,withQueue> {
  using SynchronizationUnit<Lock,withQueue>::bLock;
  using SynchronizationUnit<Lock,withQueue>::timedOut;
public:
  TimeoutSynchronizationUnit(TimerQueue& tq, Lock& bl) : TimeoutUnit(tq), SynchronizationUnit<Lock,withQueue>(bl) {}
  bool suspend(const Time& timeout, StackContext& ct = *CurrStack()) {
    SynchronizationUnit<Lock,withQueue>::prepare();
    TimeoutUnit::prepare(ct, timeout);
    unlockAndSuspend(ct, true, bLock, tQueue.lock);
    return !timedOut;
  }
  bool suspend(StackList& queue, const Time& timeout, StackContext& ct = *CurrStack()) {
    SynchronizationUnit<Lock,withQueue>::prepare(ct, queue);
    TimeoutUnit::prepare(ct, timeout);
    unlockAndSuspend(ct, true, bLock, tQueue.lock);
    return !timedOut;
  }
};

template<typename Lock>
inline void TimerQueueGeneric<Lock>::sleep(const Time& timeout) {
  TimeoutUnit tu(*this);
  Runtime::debugB( "Stack ", FmtHex(CurrStack()), " sleep ", timeout);
  tu.suspend(timeout);
}

template<typename Lock>
inline void TimerQueueGeneric<Lock>::checkExpiry(const Time& now) {
  std::list<std::pair<StackContext*,ResumeUnit*>> fireList; // defer event locks
  lock.acquire();
  for (auto it = queue.begin(); it != queue.end(); ) {
    if (it->first > now) {
      Runtime::notifyTimeout(it->first);
  break;
    }
    StackContext* t = it->second;
    ResumeUnit* ru = t->checkResumeUnit();
    if (ru) {
      it = queue.erase(it);
      fireList.push_back( {t, ru} );
    } else {
      it = next(it);
    }
  }
  lock.release();
  for (auto f : fireList) {                          // timeout lock released
    f.second->cancelSync(f.first);                   // can acquire event locks
    f.first->resume();
    Runtime::debugB( "Stack ", FmtHex(f.first), " timed out");
  }
}

template<typename Lock>
class BlockingQueue {
  StackList queue;

  BlockingQueue(const BlockingQueue&) = delete;            // no copy
  BlockingQueue& operator=(const BlockingQueue&) = delete; // no assignment

public:
  BlockingQueue() = default;
  ~BlockingQueue() { GENASSERT(empty()); }
  bool empty() const { return queue.empty(); }

  void clear(Lock& lock) {
    GENASSERT(lock.test());
    StackContext* s = queue.front();
    while (s != queue.edge()) {
      ResumeUnit* ru = s->checkResumeUnit();
      StackContext* ns = StackList::next(*s);
      if (ru) {
        ru->cancelTimeout();
        ru->cancelSync();
        StackList::remove(*s);
      }
      s = ns;
    }
    lock.release();
    while (!empty()) Pause();     // wait for timed out events to disappear
  }

  // suspend releases lock; returns 'false' on cancel/timeout/nonblocking
  bool block(Lock& lock, bool wait, const Time& timeout = Time::zero(), TimerQueue* tq = defaultTimerQueue) {
    GENASSERT(lock.test());
    if (wait) {
      if (timeout == Time::zero()) {
        SynchronizationUnit<Lock,true> su(lock);
        Runtime::debugB( "Stack ", FmtHex(CurrStack()), " blocking on ", FmtHex(&queue));
        return su.suspend(queue);
      } else {
        GENASSERT(tq);
        TimeoutSynchronizationUnit<Lock,true> tsu(*tq, lock);
        Runtime::debugB( "Stack ", FmtHex(CurrStack()), " blocking on ", FmtHex(&queue), " timeout ", timeout);
        return tsu.suspend(queue, timeout);
      }
    } else {
      lock.release();
      return false;
    }
  }

  StackContext* unblock(bool resumeNow = true) {       // not concurrency-safe; better hold lock
    for (StackContext* s = queue.front(); s != queue.edge(); s = StackList::next(*s)) {
      ResumeUnit* su = s->checkResumeUnit();
      if (su) {
        StackList::remove(*s);
        su->cancelTimeout();
        if (resumeNow) s->resume<true>();
        Runtime::debugB( "Stack ", FmtHex(&s), " resumed from ", FmtHex(&queue));
        return s;
      }
    }
    return nullptr;
  }
};

template<typename BQFront, typename BQBack = BQFront>
class StagedBlockingQueue {
  BQFront bqf;
  BQBack  bqb;
public:
  ~StagedBlockingQueue() { GENASSERT(bqf.empty()); }

  template<typename Lock>
  bool block(Lock& lock, bool wait, const Time& timeout = Time::zero(), TimerQueue* tq = defaultTimerQueue) {
    if (bqf.empty()) return bqf.block(lock, wait, timeout, tq);
    else return bqb.block(lock, wait, timeout);
  }

  void unblock() {       // not concurrency-safe; better hold lock
    if (bqf.unblock()) return;
    bqb.unblock();
  }
};

template<typename Lock, bool Binary = false, typename BQ = BlockingQueue<Lock>>
class FifoSemaphore {
  Lock lock;
  BQ bq;
  ssize_t counter;

  bool internalP(bool wait, bool yield = false, const Time& timeout = Time::zero()) {
    if (counter < 1) return bq.block(lock, wait, timeout);
    counter -= 1;
    lock.release();
    if (yield) StackContext::yield(); // yield only outside lock
    return true;
  }

public:
  explicit FifoSemaphore(ssize_t c = 0) : counter(c) {}
  // baton passing requires serialization at destruction
  ~FifoSemaphore() { ScopedLock<Lock> sl(lock); }
  bool empty() { return bq.empty(); }
  bool open() { return counter >= 1; }
  ssize_t getValue() { return counter; }

  void reset(ssize_t c = 0) {
    lock.acquire();
    counter = c;
    bq.clear(lock);
  }

  bool tryP() {
    lock.acquire();
    return internalP(false);
  }

  bool P(bool yield = false) {
    lock.acquire();
    return internalP(true, yield);
  }

  template<typename Lock2>
  bool P(Lock2& l) {
    lock.acquire();
    l.release();
    return internalP(true);
  }

  bool P(const Time& timeout) {
    lock.acquire();
    return internalP(true, false, timeout);
  }

  void V() {
    ScopedLock<Lock> al(lock);
    if (bq.unblock()) return;
    if (Binary) counter = 1;
    else counter += 1;
  }
};

template<typename Lock, bool Binary = false, typename BQ = BlockingQueue<Lock>>
class BaseSemaphore {
  Lock lock;
  BQ bq;
  size_t value;

  bool internalP(bool wait, bool yield = false, const Time& timeout = Time::zero()) {
    lock.acquire();
    while (value < 1) {
      if (!bq.block(lock, wait, timeout)) return false;
      yield = false;
      lock.acquire();
    }
    value = 0;
    lock.release();
    if (yield) StackContext::yield(); // yield only outside lock
    return true;
  }

public:
  explicit BaseSemaphore(size_t v = 0) : value(v) {}
  bool empty() { return bq.empty(); }
  bool open() { return value >= 1; }
  size_t getValue() { return value; }

  void reset(size_t v = 0) {
    lock.acquire();
    value = v;
    bq.clear(lock);
  }

  bool tryP() {
    return internalP(false);
  }

  bool P(bool yield = false) {
    return internalP(true, yield);
  }

  bool P(const Time& timeout) {
    return internalP(true, false, timeout);
  }

  void P_fake(size_t v = 1) {
    ScopedLock<Lock> al(lock);
    if (value > v) value -= v;
    else value = 0;
  }

  void V() {
    ScopedLock<Lock> al(lock);
    if (Binary) value = 1;
    else value += 1;
    bq.unblock();
  }

  void V_bulk(ProcessorResumeSet& procSet) {
    ScopedLock<Lock> al(lock);
    if (Binary) value = 1;
    else value += 1;
    StackContext* s = bq.unblock(false);
    if (s) {
      VirtualProcessor* proc = s->resume<false>();
      if (proc) procSet[proc].push(*s);
    }
  }
};

typedef BaseSemaphore<SystemLock,false> Semaphore;
typedef BaseSemaphore<SystemLock,true> BinarySemaphore;

template<typename Lock, bool OwnerLock = false, typename BQ = BlockingQueue<Lock>>
class Mutex {
protected:
  Lock lock;
  BQ bq;
  StackContext* owner;

  bool internalAcquire(bool wait, const Time& timeout = Time::zero()) {
    for (;;) {
      lock.acquire();
      if fastpath(owner == nullptr) {
        owner = CurrStack();
    break;
      } else if (owner == CurrStack()) {
        GENASSERT1(OwnerLock, FmtHex(owner));
    break;
      } else {
        if (!bq.block(lock, wait, timeout)) return false; // block() has unlocked lock
      }
    }
    lock.release();
    return true;
  }

public:
  Mutex() : owner(nullptr) {}
  bool test() { return owner != nullptr; }

  bool tryAcquire() {
    return internalAcquire(false);
  }

  bool acquire() {
    return internalAcquire(true);
  }

  bool acquire(const Time& timeout) {
    return internalAcquire(true,timeout);
  }

  void release() {
    ScopedLock<Lock> al(lock);
    GENASSERT1(owner == CurrStack(), FmtHex(owner));
    owner = nullptr;
    bq.unblock();
  }
};

template<typename BaseMutex>
class OwnerMutex : private BaseMutex {
  size_t counter;

public:
  OwnerMutex() : counter(0) {}

  size_t tryAcquire() {
    if (BaseMutex::internalAcquire(false)) return ++counter; else return 0;
  }

  size_t acquire() {
    if (BaseMutex::internalAcquire(true)) return ++counter; else return 0;
  }

  size_t acquire(const Time& timeout) {
    if (BaseMutex::internalAcquire(true,timeout)) return ++counter; else return 0;
  }

  size_t release() {
    ScopedLock<typename BaseMutex::lock> al(BaseMutex::lock);
    GENASSERT1(BaseMutex::owner == CurrStack(), FmtHex(BaseMutex::owner));
    if (--counter == 0) {
      BaseMutex::owner = nullptr;
      BaseMutex::bq.unblock();
    }
    return counter;
  }
};

// NOTE: After each successful spin, the following attempt spins twice.
//       This is a side effect of using a binary semaphore for blocking.
//       It would be more excessive with a counting semaphore.
template<typename BinSemType, size_t SpinStart, size_t SpinEnd, bool Priority = false>
class SmartLock {
  BinSemType sem;
  StackContext* owner;

  bool internalAcquire(bool wait, const Time& timeout = Time::zero()) {
    StackContext* ct = CurrStack();
    for (;;) {
      size_t spin = SpinStart;
      for (;;) {
        StackContext* expected = nullptr;
        if (__atomic_compare_exchange_n(&owner, &expected, ct, false, __ATOMIC_SEQ_CST, __ATOMIC_RELAXED)) return true;
        for (size_t i = 0; i < spin; i += 1) Pause();
        spin += spin;
        if (spin > SpinEnd) break;
      }
      if (!wait) return false;
      if (Priority) {
        mword prio = ct->getPriority();
        ct->setPriority(topPriority);
        bool ret = sem.P(timeout);
        ct->setPriority(prio);
        if (!ret) return false;
      } else {
        if (!sem.P(timeout)) return false;
      }
    }
  }

public:
  SmartLock() : sem(0), owner(nullptr) {}
  bool test() { return owner != nullptr; }

  bool tryAcquire() {
    return internalAcquire(false);
  }

  bool acquire() {
    return internalAcquire(true);
  }

  bool acquire(const Time& timeout) {
    return internalAcquire(true,timeout);
  }

  void release() {
    GENASSERT1(owner == CurrStack(), FmtHex(owner));
    __atomic_store_n(&owner, nullptr, __ATOMIC_SEQ_CST);
    sem.V();
  }
};

template<typename Lock, typename BQ = BlockingQueue<Lock>>
class MutexRW {
  Lock lock;
  BQ bqR;
  BQ bqW;
  ssize_t state;                    // -1 writer, 0 open, >0 readers

  template<bool Wait>
  bool internalAR(const Time& timeout = Time::zero()) {
    lock.acquire();
    if (state < 0 || !bqW.empty()) {
      if (!bqR.block(lock, Wait, timeout)) return false;
      lock.acquire();
      bqR.unblock();                // waiting readers can barge after writer
    }
    state += 1;
    lock.release();
    return true;
  }

  template<bool Wait>
  bool internalAW(const Time& timeout = Time::zero()) {
    lock.acquire();
    if (state != 0) {
      if (!bqW.block(lock, Wait, timeout)) return false;
      lock.acquire();
    }
    state -= 1;
    lock.release();
    return true;
  }

public:
  MutexRW() : state(0) {}

  bool tryAcquireRead() {
    return internalAR<false>();
  }

  bool acquireRead() {
    return internalAR<true>();
  }

  bool acquireRead(const Time& timeout) {
    return internalAR<true>(timeout);
  }

  bool tryAcquireWrite() {
    return internalAW<false>();
  }

  bool acquireWrite() {
    return internalAW<true>();
  }

  bool acquireWrite(const Time& timeout) {
    return internalAW<true>(timeout);
  }

  void release() {
    ScopedLock<Lock> al(lock);
    GENASSERT(state != 0);
    if (state > 0) {             // reader leaves; if open -> writer next
      state -= 1;
      if (state > 0) return;
      if (!bqW.unblock()) bqR.unblock();
    } else {                     // writer leaves -> readers next
      GENASSERT(state == -1);
      state += 1;
      if (!bqR.unblock()) bqW.unblock();
    }
  }
};

template<typename Lock, typename BQ = BlockingQueue<Lock>>
class Barrier {
  Lock lock;
  BQ bq;
  size_t counter;
  size_t target;
public:
  Barrier(size_t cnt = 1) : counter(0), target(cnt-1) { GENASSERT(cnt); }
  void clear() { lock.acquire(); bq.clear(lock); }
  void init(size_t cnt) {
    GENASSERT(target == 0);
    target = cnt - 1;
  }
  bool wait() {
    lock.acquire();
    if (counter == target) {
      while (bq.unblock());
      counter = 0;
      lock.release();
      return true;
    } else {
      counter += 1;
      bq.block(lock, true);
      return false;
    }
  }
};

// assume caller holds lock
template<typename Lock, typename BQ = BlockingQueue<Lock>>
class Condition {
  BQ bq;
public:
  bool empty() { return bq.empty(); }
  void reset(Lock& lock) { bq.clear(lock); }
  bool wait(Lock& lock) { return bq.block(lock, true); }
  bool wait(Lock& lock, const Time& to) { return bq.block(lock, true, to); }
  template<bool Broadcast = false>
  void signal() { while (bq.unblock() && Broadcast); }
};

// cf. condition variable: assume caller to wait/post holds lock
template<typename Lock>
class SynchronizedFlag {
public:
  enum State { Running = 0, Dummy = 1, Posted = 2, Detached = 4 };
protected:
  union {                             // 'waiter' set <-> 'state == Waiting'
    StackContext* waiter;
    State         state;
  };

public:
  static const State Invalid = Dummy; // dummy bit never set (for ManagedArray)
  SynchronizedFlag(State s = Running) : state(s) {}
  void reset()          { state = Running; }
  bool posted()   const { return state == Posted; }
  bool detached() const { return state == Detached; }

  bool wait(Lock& bl) {          // returns false, if detached
    if (state == Running) {
      SynchronizationUnit<Lock,false> su(bl);
      waiter = CurrStack();
      su.suspend(false, *waiter);
      bl.acquire();                   // reacquire lock to check state
    }
    if (state == Posted) return true;
    if (state == Detached) return false;
    GENABORT1(FmtHex(state));
  }

  bool post() {                       // returns false, if already detached
    GENASSERT(state != Posted);       // check for spurious posts
    if (state == Detached) return false;
    if (state != Running) waiter->resume();
    state = Posted;
    return true;
  }

  void detach() {                     // returns false, if already posted or detached
    GENASSERT(state != Detached && state != Posted);
    if (state != Running) waiter->resume();
    state = Detached;
  }
};

// cf. condition variable: assume caller to wait/post holds lock
template<typename Runner, typename Result, typename Lock>
class Joinable : public SynchronizedFlag<Lock> {
  using Baseclass = SynchronizedFlag<Lock>;
protected:
  union {
    Runner* runner;
    Result  result;
  };

public:
  Joinable(Runner* t) : runner(t) {}

  bool wait(Lock& bl, Result& r) {
    bool retcode = Baseclass::wait(bl);
    r = retcode ? result : 0; // result is available after returning from wait
    return retcode;
  }

  bool post(Result r) {
    bool retcode = Baseclass::post();
    if (retcode) result = r;  // still holding lock while setting result
    return retcode;
  }

  Runner* getRunner() const { return runner; }
};

template<typename Lock>
class SyncPoint : public SynchronizedFlag<Lock> {
  using Baseclass = SynchronizedFlag<Lock>;
  typedef typename SynchronizedFlag<Lock>::State State;
  Lock lock;
public:
  SyncPoint(State s = Baseclass::Running) : Baseclass(s) {}

  void reset() {
    ScopedLock<Lock> al(lock);
    Baseclass::reset();
  }

  bool wait() {
    ScopedLock<Lock> al(lock);
    return Baseclass::wait(lock);
  }

  bool post() {
    ScopedLock<Lock> al(lock);
    return Baseclass::post();
  }

  void detach() {
    ScopedLock<Lock> al(lock);
    Baseclass::detach();
  }
};

#endif /* _BlockingSync_h_ */
