/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/Scheduler.h"
#include "runtime/RuntimeImpl.h"

// BG enqueue -> find idle "real" processor to run stack
// idleLock synchronizes between 'addIdleProcessor' and 'findIdleProcessor'
void VirtualProcessor::wakeUp() {
  BaseProcessor* idleProc = currScheduler->findIdleProcessor();
  GENASSERT1(idleProc != this, FmtHex(this));
  if (idleProc) idleProc->wakeUp();
}

void BaseProcessor::setScheduler(Scheduler& sched) {
  currScheduler = &sched;
  currScheduler->insert(*this);
}

#if TESTING_ENABLE_VP_SPIN
static const unsigned int spinMax = 32;
#else
static const unsigned int spinMax = 0;
#endif

bool BaseProcessor::busyLoop() {
  unsigned int spin = 0;
  for (;;) {
    // search for work
    if (StackContext::yield()) spin = 0;
    // check for termination
    if (idleTerminate && empty()) return false;
    // check for migration
    Scheduler* next = __atomic_exchange_n( &nextScheduler, nullptr, __ATOMIC_RELAXED );
    if (next) {
      currScheduler->remove(*this);
      setScheduler(*next);
    }
    // report idle and wait, if enough spins have passed
    if (++spin > spinMax) return true;
  }
}
