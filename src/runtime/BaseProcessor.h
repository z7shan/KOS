/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _BaseProcessor_h_
#define _BaseProcessor_h_ 1

#include "runtime/StackContext.h"

class BasePoller;
class Scheduler;

class ReadyQueue {
#if TESTING_LOCKFREE_READYQUEUE
#if TESTING_BLOCKING_LOCKFREE
  StackMPSC       queue;
#else
  StackMPSC       queue[maxPriority];
#endif
#else /* TESTING_LOCKFREE_READYQUEUE */
  StackQueue      queue[maxPriority];
#endif

  volatile size_t count;

  ReadyQueue(const ReadyQueue&) = delete;            // no copy
  ReadyQueue& operator=(const ReadyQueue&) = delete; // no assignment

public:
  ReadyQueue() : count(0) {}
  size_t load() const { return count; }

  bool singleEnqueue(StackContext& s) {
#if TESTING_LOCKFREE_READYQUEUE
#if TESTING_BLOCKING_LOCKFREE
    __atomic_add_fetch(&count, 1, __ATOMIC_RELAXED);
    return queue.push(s);
#else
    queue[s.getPriority()].push(s);
    return __atomic_add_fetch(&count, 1, __ATOMIC_RELAXED) == 1;
#endif /* TESTING_BLOCKING_LOCKFREE */
#else
    ScopedLock<SystemLock> sl(readyLock);
    queue[s.getPriority()].push(s);
    count += 1;
    return count == 1;
#endif
  }

  bool bulkEnqueue(ResumeQueue& rq) {
    GENASSERT(rq.count);
#if TESTING_LOCKFREE_READYQUEUE
#if TESTING_BLOCKING_LOCKFREE
    __atomic_add_fetch(&count, rq.count, __ATOMIC_RELAXED);
    return queue.transferAllFrom(rq.queue);
#else
    bool retcode = (__atomic_fetch_add(&count, rq.count, __ATOMIC_RELAXED) == 0);
    for (mword p = 0; p < maxPriority; p += 1) {
      queue[p].transferAllFrom(rq.queue[p]);
    }
    return retcode;
#endif /* TESTING_BLOCKING_LOCKFREE */
#else
    ScopedLock<SystemLock> sl(readyLock);
    for (mword p = 0; p < maxPriority; p += 1) {
      queue[p].transferAllFrom(rq.queue[p]);
    }
    count += rq.count;
    return count == rq.count;
#endif
  }

  StackContext* dequeue() {
#if TESTING_LOCKFREE_READYQUEUE
#if TESTING_BLOCKING_LOCKFREE
    StackContext* s = queue.pop();
    if fastpath(s) {
      __atomic_sub_fetch(&count, 1, __ATOMIC_RELAXED);
      return s;
    }
#else
    while (count > 0) {                      // spin until push completes
      for (mword p = 0; p < maxPriority; p += 1) {
        StackContext* s = queue[p].pop();
        if (s) {
          __atomic_sub_fetch(&count, 1, __ATOMIC_RELAXED);
          return s;
        }
      }
    }
#endif /* TESTING_BLOCKING_LOCKFREE */
#else
    for (mword p = 0; p < maxPriority; p += 1) {
      if (!queue[p].empty()) {
        if (Affinity || !queue[p].front()->getAffinity()) {
          count -= 1;
          return queue[p].pop();
        }
      }
    }
#endif
    return nullptr;
  }
};

class VirtualProcessor {
protected:
  SystemLock      readyLock;
  ReadyQueue      readyQueue;
  volatile size_t stackCount;
  volatile size_t lbCount;
  Scheduler*      currScheduler;
  ProcessorStats* stats;

public:
  VirtualProcessor(Scheduler* sched) : stackCount(0), lbCount(1), currScheduler(sched) {
    stats = new ProcessorStats(this);
  }

  size_t empty() const { return stackCount == 1; } // only idle stack left
  size_t load() const { return readyQueue.load(); }
  Scheduler& getScheduler() { GENASSERT(currScheduler); return *currScheduler; }
  virtual void wakeUp();

  bool lbtest(size_t cnt) {
#if TESTING_ALWAYS_MIGRATE
    return true;
#else
    return (readyQueue.load() > 1) && (lbCount % cnt == 0);
#endif
  }

  void addStackContext(_friend<StackContext>) {
    __atomic_add_fetch(&stackCount, 1, __ATOMIC_RELAXED);
  }
  void removeStackContext(_friend<StackContext>) {
    __atomic_sub_fetch(&stackCount, 1, __ATOMIC_RELAXED);
  }

  void singleResume(StackContext& s, _friend<StackContext>) {
    GENASSERT1(s.getPriority() < maxPriority, s.getPriority());
    Runtime::debugS("Stack ", FmtHex(&s), " queueing on ", FmtHex(this));
    stats->enq.count();
    if (readyQueue.singleEnqueue(s)) wakeUp();
  }

  // No locking for ResumeQueue! The caller better knows what it is doing.
  void bulkResume(ResumeQueue& rq, _friend<BasePoller>) {
    Runtime::debugS(rq.count, " stacks bulk queueing on ", FmtHex(this));
    stats->bulk.add(rq.count);
    if (readyQueue.bulkEnqueue(rq)) wakeUp();
  }

  StackContext* dequeueSafe(_friend<Scheduler>) {
    readyLock.acquire();
    StackContext* sc = readyQueue.dequeue();
    readyLock.release();
    if (sc) stats->deq.count();
    return sc;
  }
};

class BaseProcessor;
typedef IntrusiveRing<BaseProcessor,0,2> ProcessorRing;
typedef IntrusiveList<BaseProcessor,1,2> ProcessorList;

class BaseProcessor : public ProcessorList::Link, public VirtualProcessor {
protected:
  volatile bool idleTerminate;
  volatile bool onIdleList;
  StackContext* idleStack;
  Scheduler*    nextScheduler;

  void setupIdle(StackContext* is) {
    idleStack = is;
    idleStack->setPriority(idlePriority);
  }

  bool busyLoop();
  void setScheduler(Scheduler& sched);

public:
  BaseProcessor(Scheduler* sched = nullptr) : VirtualProcessor(sched),
    idleTerminate(false), idleStack(nullptr), nextScheduler(nullptr) {}

  volatile bool& idleList(_friend<Scheduler>) { return onIdleList; }

  void changeScheduler(Scheduler& sched) {
    GENASSERT(currScheduler);
    nextScheduler = &sched;   // set up scheduler migration when idle
    wakeUp();                 // wake up idle loop (just in case)
  }

  StackContext* dequeue(_friend<Scheduler> fs) {
#if TESTING_LOCKFREE_READYQUEUE
    StackContext* sc = readyQueue.dequeue();
    if fastpath(sc) stats->deq.count();
#else
    StackContext* sc = dequeueSafe(fs);
#endif
    if fastpath(sc) lbCount += 1;
    else lbCount = 1;
    return sc;
  }

  StackContext* idle(_friend<Scheduler>) {
    return idleStack;
  }
};

#endif /* _BaseProcessor_h_ */
