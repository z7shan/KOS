/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/RuntimeImpl.h"
#include "runtime/Scheduler.h"
#include "runtime/StackContext.h"

StackContext::StackContext(VirtualProcessor& proc, bool a)
: currProcessor(&proc), nextProcessor(nullptr), affinity(a),
  priority(defPriority), state(Running), resumeUnit(nullptr) {
  currProcessor->addStackContext(_friend<StackContext>());
}

StackContext::StackContext()
: StackContext(CurrProcessor()) {}

StackContext::StackContext(Scheduler& sched, bool bg)
: StackContext(sched.placement(_friend<StackContext>(), bg), bg) {}

void StackContext::destructor() {
  GENASSERT1(state == Running, FmtHex(this));
  GENASSERT1(resumeUnit == nullptr, FmtHex(this));
  GENASSERT1(nextProcessor == nullptr, FmtHex(this));
  currProcessor->removeStackContext(_friend<StackContext>());
}

template<StackContext::SwitchCode Code>
inline bool StackContext::switchStack() {
  // various checks
  static_assert(Code == Yield || Code == Migrate || Code == Suspend || Code == Terminate, "wrong SwitchCode");
  CHECK_PREEMPTION(0);
  GENASSERTN(this == CurrStack(), FmtHex(this), ' ', FmtHex(CurrStack()));

  // pick next thread
  StackContext* nextStack = CurrScheduler().schedule(CurrProcessor(), _friend<StackContext>());
  GENASSERT(nextStack);
  if (Code == Yield && nextStack->priority == idlePriority) {
    Runtime::noStackSwitch(this);
    return false;
  }
  Runtime::debugS("Stack switch <", char(Code), ">: ", FmtHex(this), " to ", FmtHex(nextStack), " on ", FmtHex(&CurrProcessor()));
  GENASSERTN(nextStack != this, FmtHex(this), ' ', FmtHex(nextStack));

  // context switch
  Runtime::preStackSwitch(this, nextStack);
  switch (Code) {
    case Yield:     stackSwitch(this, postYield,     &stackPointer, nextStack->stackPointer); break;
    case Migrate:   stackSwitch(this, postMigrate,   &stackPointer, nextStack->stackPointer); break;
    case Suspend:   stackSwitch(this, postSuspend,   &stackPointer, nextStack->stackPointer); break;
    case Terminate: stackSwitch(this, postTerminate, &stackPointer, nextStack->stackPointer); break;
  }
  stackPointer = 0; // mark stack in use
  Runtime::postStackSwitch(this);
  return true;
}

// if not idle stack, resume right away
void StackContext::postYield(StackContext* prevStack) {
  CHECK_PREEMPTION(0);
  if fastpath(prevStack->priority < idlePriority) prevStack->resumeInternal();
}

// resume directly to (new) currProcessor without load balancing
void StackContext::postMigrate(StackContext* prevStack) {
  CHECK_PREEMPTION(0);
  prevStack->currProcessor->singleResume(*prevStack, _friend<StackContext>());
}

// if resumption already triggered -> resume right away
void StackContext::postSuspend(StackContext* prevStack) {
  CHECK_PREEMPTION(0);
  State prevState = Prepared;
  bool suspended = __atomic_compare_exchange_n( &prevStack->state, &prevState, Suspended, false, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST );
  if (!suspended) {
    GENASSERT1(prevState == Running, FmtHex(prevStack));
    prevStack->resumeInternal();
  }
}

// destroy stack
void StackContext::postTerminate(StackContext* prevStack) {
  CHECK_PREEMPTION(0);
  Runtime::stackDestroy(prevStack);
}

// a new thread/stack starts in stubInit() and then jumps to this routine
extern "C" void invokeStack(funcvoid4_t func, ptr_t arg1, ptr_t arg2, ptr_t arg3, ptr_t arg4) {
  CHECK_PREEMPTION(0);
  Runtime::EnablePreemption();
  func(arg1, arg2, arg3, arg4);
  Runtime::DisablePreemption();
  CurrStack()->terminate();
}

void StackContext::resumeInternalPrep() {
  GENASSERT1(currProcessor, FmtHex(this));
  if (nextProcessor) {
    VirtualProcessor* next = __atomic_exchange_n( &nextProcessor, nullptr, __ATOMIC_RELAXED );
    if (next) {
      currProcessor->removeStackContext(_friend<StackContext>());
      currProcessor = next;
    }
  }
#if !TESTING_DISABLE_LOADBALANCER
  if (affinity || !currProcessor->lbtest(currProcessor->getScheduler().procCount())) return;
  VirtualProcessor& proc = currProcessor->getScheduler().loadBalance(*currProcessor, _friend<StackContext>());
  if (&proc != currProcessor) changeProcessor(proc);
#endif
}

void StackContext::resumeInternal() {
  resumeInternalPrep();
  currProcessor->singleResume(*this, _friend<StackContext>());
}

bool StackContext::yield() {
  StackContext* s = CurrStack();
  CHECK_PREEMPTION(1);          // expect preemption enabled
  Runtime::DisablePreemption();
  bool retcode = s->switchStack<Yield>();
  Runtime::EnablePreemption();
  return retcode;
}

bool StackContext::preempt() {
  StackContext* s = CurrStack();
  CHECK_PREEMPTION(0);          // expect preemption disabled
  return s->switchStack<Yield>();
}

void StackContext::terminate() {
  StackContext* s = CurrStack();
  CHECK_PREEMPTION(0);          // expect preemption disabled
  s->switchStack<Terminate>();
  unreachable();
}

void StackContext::suspend(_friend<ResumeUnit>) {
  CHECK_PREEMPTION(0);          // expect preemption disabled
  switchStack<Suspend>();
}

inline void StackContext::setMigrationTarget(VirtualProcessor& next) {
  next.addStackContext(_friend<StackContext>());
  VirtualProcessor* old = __atomic_exchange_n( &nextProcessor, &next, __ATOMIC_RELAXED );
  if (old) old->removeStackContext(_friend<StackContext>());
}

inline void StackContext::changeProcessor(VirtualProcessor& proc) {
  currProcessor->removeStackContext(_friend<StackContext>());
  currProcessor = &proc;
  currProcessor->addStackContext(_friend<StackContext>());
}

StackContext* StackContext::setAffinity(VirtualProcessor& proc) {
  setAffinity();
  setMigrationTarget(proc);
  return this;
}

StackContext* StackContext::migrate(Scheduler& sched) {
  setAffinity(false);
  setMigrationTarget(sched.placement(_friend<StackContext>()));
  return this;
}

// migrate from regular processor to another on the same scheduler; adjust both stackCounts
void StackContext::migrateSelf(Scheduler& sched) {
  StackContext* sc = CurrStack();
  sc->setAffinity(false);
  sc->changeProcessor(sched.placement(_friend<StackContext>()));
  Runtime::DisablePreemption();
  sc->switchStack<Migrate>();
  Runtime::EnablePreemption();
}

// migrate from regular processor to specific scheduler, don't change either stackCount
VirtualProcessor& StackContext::migrateSelf(Scheduler& sched, _friend<EventEngine>) {
  StackContext* sc = CurrStack();
  VirtualProcessor* proc = sc->currProcessor;
  sc->currProcessor = &sched.placement(_friend<StackContext>());
  Runtime::DisablePreemption();
  sc->switchStack<Migrate>();
  Runtime::EnablePreemption();
  return *proc;
}

// migrate back from dedicated to previous processor, don't change either stackCount
void StackContext::migrateSelf(VirtualProcessor& proc, _friend<EventEngine>) {
  StackContext* sc = CurrStack();
  sc->currProcessor = &proc;
  Runtime::DisablePreemption();
  sc->switchStack<Migrate>();
  Runtime::EnablePreemption();
}
