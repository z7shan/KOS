# to automatically load this when libfibre.so is loaded:
# echo add-auto-load-safe-path . >> $HOME/.gdbinit

import gdb

class FibreSupport():
    def __init__(self):
        FibreSupport.list = []
        FibreSupport.saved = False
        if (gdb.lookup_symbol("_globalStackList")[0] == None):
            print()
            print("WARNING: no fibre debugging support - did you enable TESTING_ENABLE_DEBUGGING?")
            print()

    def stop_handler(event):
        if (gdb.lookup_symbol("_globalStackList")[0] == None):
            return
        # save register context for later continuation
        FibreSupport.rsp = str(gdb.parse_and_eval("$rsp")).split(None, 1)[0]
        FibreSupport.rbp = str(gdb.parse_and_eval("$rbp")).split(None, 1)[0]
        FibreSupport.rip = str(gdb.parse_and_eval("$rip")).split(None, 1)[0]
        FibreSupport.saved = True
        # traverse runtime stack list to build internal list of fibres
        _globalStackList = gdb.parse_and_eval("_globalStackList")
        first = _globalStackList['anchor'].address
        next = _globalStackList['anchor']['link'][1]['next']
        while (next != first):
            FibreSupport.list.append(next)
            next = next['link'][1]['next']

    def cont_handler(event):
        FibreSupport.list = []

    # ideally restore() would be hooked up with cont_handler,
    # but gdb currently fails with segmentation fault when trying
    def restore():
        if (FibreSupport.saved):
            FibreSupport.prep_frame()
            # restore original register context
            gdb.execute("set $rsp = " + str(FibreSupport.rsp))
            gdb.execute("set $rbp = " + str(FibreSupport.rbp))
            gdb.execute("set $rip = " + str(FibreSupport.rip))
            FibreSupport.saved = False

    def prep_frame():
        # walk stack down to innermost frame
        currframe = gdb.selected_frame()
        frame = currframe
        while (frame != gdb.newest_frame()):
            frame = frame.newer()
        frame.select()
        return currframe

    def set_fibre(arg):
        # if current pthread: use current register context
        if (arg == gdb.parse_and_eval("Context::currStack")):
            return True
        # retrieve fibre's register context
        ftype = gdb.lookup_type('Fibre').pointer()
        ptr = gdb.Value(arg).cast(ftype)
        rsp = ptr['stackPointer']
        if (rsp == 0):
            print("cannot access stack pointer - active in different thread?")
            return False
        ptype = gdb.lookup_type('uintptr_t').pointer()
        rsp = rsp + 40            # cf. STACK_PUSH in src/generic/regsave.h
        rbp = gdb.Value(rsp).cast(ptype).dereference()
        rsp = rsp + 8
        rip = gdb.Value(rsp).cast(ptype).dereference()
        # enable fibre's register context
        gdb.execute("set $rsp = " + str(rsp))
        gdb.execute("set $rbp = " + str(rbp))
        gdb.execute("set $rip = " + str(rip))
        return True

    def backtrace(arg):
        currframe = FibreSupport.prep_frame()
        # save register context
        tmprsp = str(gdb.parse_and_eval("$rsp")).split(None, 1)[0]
        tmprbp = str(gdb.parse_and_eval("$rbp")).split(None, 1)[0]
        tmprip = str(gdb.parse_and_eval("$rip")).split(None, 1)[0]
        # execute backtrace, if possible
        if (FibreSupport.set_fibre(arg)):
            gdb.execute("backtrace")
        # restore register context
        gdb.execute("set $rsp = " + str(tmprsp))
        gdb.execute("set $rbp = " + str(tmprbp))
        gdb.execute("set $rip = " + str(tmprip))
        # restore stack frame
        currframe.select()

class InfoFibres(gdb.Command):
    def __init__(self):
        super(InfoFibres, self).__init__("info fibres", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        curr = gdb.parse_and_eval("Context::currStack")
        for i in range(len(FibreSupport.list)):
            print(str(i), "\tFibre", str(FibreSupport.list[i]), end='')
            if (FibreSupport.list[i] == curr):
                print(" (*)")
            else:
                print()

class Fibre(gdb.Command):
    def __init__(self):
        super(Fibre, self).__init__("fibre", gdb.COMMAND_SUPPORT, gdb.COMPLETE_NONE, True)

class FibrePtr(gdb.Command):
    def __init__(self):
        super(FibrePtr, self).__init__("fibre ptr", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        FibreSupport.backtrace(gdb.parse_and_eval(arg))

class FibreIdx(gdb.Command):
    def __init__(self):
        super(FibreIdx, self).__init__("fibre idx", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        index = int(gdb.parse_and_eval(arg))
        if (index >= len(FibreSupport.list)):
            print("fibre", index, "does not exist")
            return
        FibreSupport.backtrace(FibreSupport.list[index])

class FibreSetPtr(gdb.Command):
    def __init__(self):
        super(FibreSetPtr, self).__init__("fibre setptr", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        FibreSupport.prep_frame()
        FibreSupport.set_fibre(gdb.parse_and_eval(arg))

class FibreSetIdx(gdb.Command):
    def __init__(self):
        super(FibreSetIdx, self).__init__("fibre setidx", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        index = int(gdb.parse_and_eval(arg))
        if (index >= len(FibreSupport.list)):
            print("fibre", index, "does not exist")
            return
        FibreSupport.prep_frame()
        FibreSupport.set_fibre(FibreSupport.list[index])

class FibreReset(gdb.Command):
    def __init__(self):
        super(FibreReset, self).__init__("fibre reset", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        FibreSupport.restore()

FibreSupport()
InfoFibres()
Fibre()
FibrePtr()
FibreIdx()
FibreSetPtr()
FibreSetIdx()
FibreReset()
gdb.events.stop.connect(FibreSupport.stop_handler)
gdb.events.cont.connect(FibreSupport.cont_handler)
