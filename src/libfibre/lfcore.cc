/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "libfibre/Fibre.h"
#include "libfibre/EventEngine.h"

// debug output lock
SystemLock* _lfDebugOutputLock = nullptr;                            // lfbasics.h

// instance definitions for Context members
thread_local SystemProcessor* volatile Context::currProc  = nullptr; // lfbasics.h
thread_local StackContext*    volatile Context::currStack = nullptr; // lfbasics.h

// noinline routines for Context
void Context::setCurrStack(StackContext& s, _friend<Runtime>) { currStack = &s; }
SystemProcessor* Context::self()       { return currProc; }
StackContext*    Context::CurrStack()  { return currStack; }

// global pointers
EventEngine*     _lfEventEngine    = nullptr; // EventEngine.h
TimerQueue*      defaultTimerQueue = nullptr; // BlockingSync.h

#if TESTING_ENABLE_DEBUGGING
SystemLock*      _globalStackLock  = nullptr; // StackContext.h
GlobalStackList* _globalStackList  = nullptr; // StackContext.h
#endif

// make these pointers global static to enable gdb access
static Fibre*           _lfMainFibre     = nullptr;
static SystemProcessor* _lfMainProcessor = nullptr;
static PollerScheduler* _lfMainScheduler = nullptr;

#if TESTING_ENABLE_STATISTICS
std::list<StatsObject*>* StatsObject::lst = nullptr;
#endif

#if TESTING_ENABLE_ASSERTIONS
static SystemLock _abortLock;
void _SYSCALLabortLock() { _abortLock.acquire(); }
void _SYSCALLabortUnlock() { _abortLock.release(); }
#endif

// ******************** EVENT POLLING ********************

// drain all events with non-blocking epoll_wait
inline void BasePoller::poll(int evcnt) {
  int currPoll = firstPoll;
  for (;;) {
    stats->events.add(evcnt);
    ProcessorResumeSet procSet;
    for (int e = 0; e < evcnt; e += 1) {
#if __FreeBSD__
      struct kevent& ev = events[e];
      if (ev.filter == EVFILT_READ || ev.filter == EVFILT_TIMER) {
        _lfEventEngine->unblock<true>(ev.ident, procSet, _friend<BasePoller>());
      } else if (ev.filter == EVFILT_WRITE) {
        _lfEventEngine->unblock<false>(ev.ident, procSet, _friend<BasePoller>());
      }
#else // __linux__ below
      epoll_event& ev = events[e];
      if (ev.events & (EPOLLIN | EPOLLPRI | EPOLLERR | EPOLLHUP)) {
        _lfEventEngine->unblock<true>(ev.data.fd, procSet, _friend<BasePoller>());
      }
      if (ev.events & (EPOLLOUT | EPOLLRDHUP | EPOLLERR)) {
        _lfEventEngine->unblock<false>(ev.data.fd, procSet, _friend<BasePoller>());
      }
#endif
    }
    for (std::pair<VirtualProcessor* const,ResumeQueue>& p : procSet) {
      p.first->bulkResume(p.second, _friend<BasePoller>());
    }
    if (evcnt < currPoll) break;
#if TESTING_EXPONENTIAL_EPOLL
    currPoll = (currPoll <= maxPoll/2) ? (currPoll * 2) : maxPoll;
#endif
#if __FreeBSD__
    static const timespec ts = Time::zero();
    evcnt = SYSCALLIO(kevent(pollFD, nullptr, 0, events, currPoll, &ts));
#else // __linux__ below
    evcnt = SYSCALLIO(epoll_wait(pollFD, events, currPoll, 0));
#endif
  }
}

template<typename T>
inline void PollerThread::pollLoop() {
  while (!pollTerminate) {
    if (!reinterpret_cast<T*>(this)->wait(_friend<PollerThread>())) usleep(100);  // avoid trickle loop
#if __FreeBSD__
    int evcnt = kevent(pollFD, nullptr, 0, events, firstPoll, nullptr);
#else // __linux__ below
    int evcnt = epoll_wait(pollFD, events, firstPoll, -1); // blocking epoll
#endif
    if (evcnt >= 0) poll(evcnt);
    else { GENASSERT1(errno == EINTR, errno); }            // gracefully handle EINTR
  }
}

void* PollerThread::pollLoopSetup(void* This) {
  reinterpret_cast<PollerThread*>(This)->pollLoop<PollerThread>();
  return nullptr;
}

#if TESTING_POLLER_FIBRES
Poller::Poller(PollerScheduler& sched) {
  pollFibre = new Fibre(sched, defaultStackSize, true);
  pollFibre->run(pollLoopSetup, this);
}

void Poller::stop() {
  pollTerminate = true; // set termination flag, then unblock -> terminate
  _lfEventEngine->unblockPoller(pollFD, _friend<Poller>());
  delete pollFibre;
}

inline void Poller::pollLoop() {
  _lfEventEngine->masterPoller.registerFD<false>(pollFD);
  while (!pollTerminate) {
    _lfEventEngine->block<true>(pollFD);
#if __FreeBSD__
    static const timespec ts = Time::zero();
    int evcnt = SYSCALLIO(kevent(pollFD, nullptr, 0, events, firstPoll, &ts));
#else // __linux__ below
    int evcnt = SYSCALLIO(epoll_wait(pollFD, events, firstPoll, 0));
#endif
    if (evcnt >= 0) poll(evcnt);
    else { GENASSERT1(errno == EINTR, errno); }            // gracefully handle EINTR
  }
}

void Poller::pollLoopSetup(void* This) {
  reinterpret_cast<Poller*>(This)->pollLoop();
}

#else // TESTING_POLLER_FIBRES

void* Poller::pollLoopSetup(void* This) {
  reinterpret_cast<Poller*>(This)->pollLoop<Poller>();
  return nullptr;
}

inline bool Poller::wait(_friend<PollerThread>) {
  return sched.waitForPoll();
}

#endif // TESTING_POLLER_FIBRES

// ******************** TIMER HANDLING ********************

inline void EventEngine::timerLoop() {
  while (!timerTerminate) {
#if __FreeBSD__
    block<true>(timerFD);
#else // __linux__ below
    uint64_t count;
    SYSCALLIO(lfInput(read, timerFD, (void*)&count, sizeof(count)));
#endif
    Time currTime;
    SYSCALL(clock_gettime(CLOCK_REALTIME, &currTime));
    defaultTimerQueue->checkExpiry(currTime);
  }
}

void EventEngine::timerLoopSetup(void* This) {
  reinterpret_cast<EventEngine*>(This)->timerLoop();
}

void _lfNotifyTimeout(const Time& timeout) {
  _lfEventEngine->notifyTimeout(timeout);
}

// ******************** BOOTSTRAP ********************

// bootstrap counter definition
std::atomic<int> _Bootstrapper::counter(0);

_Bootstrapper::_Bootstrapper() {
  if (++counter == 1) {
#if TESTING_ENABLE_STATISTICS
    StatsObject::lst = new std::list<StatsObject*>;
#endif
    // create lock for debug output
    _lfDebugOutputLock = new SystemLock;
#if TESTING_ENABLE_DEBUGGING
    // create global fibre list
    _globalStackLock = new SystemLock;
    _globalStackList = new GlobalStackList;
#endif
    // create default timer queue
    defaultTimerQueue = new TimerQueue;
    // start event demultiplexing
    _lfEventEngine = new EventEngine;
    // create default scheduler -> includes poller
    _lfMainScheduler = new PollerScheduler;
    // create main SP
    _lfMainProcessor = new SystemProcessor(*_lfMainScheduler, _friend<_Bootstrapper>());
    // create main fibre and main SP's idle fibre using dedicated interface
    _lfMainFibre = _lfMainProcessor->init(_friend<_Bootstrapper>());
    // start timer handling in event engine -> needs main scheduler/processor
    _lfEventEngine->startTimerHandling();
  }
}

_Bootstrapper::~_Bootstrapper() {
  if (--counter == 0) {
//    delete _lfMainFibre;
//    delete _lfMainScheduler;
//    delete _lfEventEngine;
//    delete defaultTimerQueue;
//    delete _lfDebugOutputLock;
#if TESTING_ENABLE_STATISTICS
      StatsObject::printAll(std::cout);
      delete StatsObject::lst;
#endif
  }
}
