/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _SystemProcessor_h_
#define _SystemProcessor_h_ 1

#include "runtime/Scheduler.h"
#include "libfibre/Poller.h"

#include <semaphore.h>

class Fibre;
class EventEngine;

class SystemProcessor : public Context, public BaseProcessor {
  pthread_t     sysThread;
  sem_t         idleSem;

  inline void   idleLoop();
  static void   idleLoopSetupFibre(void*);
  static void*  idleLoopSetupPthread(void*);

protected:
  // dedicated constructor for C interface using existing pthread
  SystemProcessor(Scheduler& sched, funcvoid1_t func, ptr_t arg);

public:
  // regular constructors
  SystemProcessor();
  SystemProcessor(Scheduler& sched);

  // dedicated constructor for bootstrap
  SystemProcessor(Scheduler& sched, _friend<_Bootstrapper>);
  // dedicated initialization routine for bootstrap
  Fibre* init(_friend<_Bootstrapper>);

  // destructor moves all fibres and ends pthread
  virtual ~SystemProcessor();

  pthread_t getSysID() { return sysThread; }

  virtual void wakeUp() {
    stats->wake.count();
    SYSCALL(sem_post(&idleSem));
  }
};

static inline SystemProcessor& CurrProcessor() {
  SystemProcessor* proc = Context::self();
  GENASSERT(proc);
  return *proc;
}

class FibreScheduler : public Scheduler {
  volatile bool   paused;
  SystemSemaphore pauseWaitSem;
  SystemSemaphore pauseContSem;

#if !TESTING_POLLER_FIBRES
  size_t          pollThreshold;
  SystemCondition pollCond;
#endif
public:
  FibreScheduler(size_t t) : paused(false), pauseContSem(1)
#if !TESTING_POLLER_FIBRES
  , pollThreshold(t)
#endif
  {}

#if !TESTING_POLLER_FIBRES && !TESTING_POLLER_IDLETIMEDWAIT
  ~FibreScheduler() {
    ScopedLock<SystemLock> al(idleLock);
    pollThreshold = 0;
    pollCond.signal();
  }
#endif

#if !TESTING_POLLER_FIBRES
  size_t addIdleProcessor(BaseProcessor& proc) {
    if (paused) {
      pauseWaitSem.V();
      pauseContSem.P();
      pauseContSem.V();
    }
    size_t c = Scheduler::addIdleProcessor(proc);
#if TESTING_POLLER_IDLEWAIT
    if (c == pollThreshold) pollCond.signal();
#endif
    return c;
  }
#endif // !TESTING_POLLER_FIBRES

#if !TESTING_POLLER_FIBRES
  bool waitForPoll() {
#if TESTING_POLLER_IDLEWAIT
    idleLock.acquire();
    if (idleCount < pollThreshold) {
#if TESTING_POLLER_IDLETIMEDWAIT
      Time t;
      SYSCALL(clock_gettime(CLOCK_REALTIME, &t));
      t += Time(0,50000000);                            // 50 ms = 50,000,000 ns;
      pollCond.block(idleLock, true, t);
#else
      pollCond.block(idleLock, true);
#endif // TESTING_POLLER_IDLETIMEDWAIT
      return true;
    }
    idleLock.release();
#endif // TESTING_POLLER_IDLEWAIT
    return false;
  }
#endif // !TESTING_POLLER_FIBRES

  void resume() {
    paused = false;
    pauseContSem.V();
  }

  void pause() {
    pauseContSem.P();
    paused = true;
    idleLock.acquire();
    for (;;) {
      BaseProcessor* p = findIdleProcessorLocked();
      if (p) p->wakeUp();
      else break;
    }
    idleLock.release();
    size_t start = (&CurrProcessor().getScheduler() == this) ? 1 : 0;
    for (size_t i = start; i < procCount(); i += 1) pauseWaitSem.P();
  }
};

class PollerScheduler : public FibreScheduler {
  Poller poller;
public:
  PollerScheduler(size_t t = 1) : FibreScheduler(t), poller(*this) {}
  ~PollerScheduler() {
#if TESTING_POLLER_FIBRES
    GENASSERT(poller.stopped() || procCount() > 0);
#endif
  }
  Poller& getPoller() { return poller; }

  void stopPoller() {
#if TESTING_POLLER_FIBRES
    GENASSERT(!poller.stopped() && procCount() > 0);
#endif
    poller.stop();
  }
};

static inline PollerScheduler& CurrScheduler() {
  return reinterpret_cast<PollerScheduler&>(CurrProcessor().getScheduler());
}

static inline Poller& CurrPoller() {
  return CurrScheduler().getPoller();
}

#endif /* _SystemProcessor_h_ */
