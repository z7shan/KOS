/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/RuntimeImpl.h"
#include "libfibre/SystemProcessor.h"

#include <limits.h>       // PTHREAD_STACK_MIN

SystemProcessor::SystemProcessor() : SystemProcessor(CurrScheduler()) {}

SystemProcessor::SystemProcessor(Scheduler& sched) {
  setScheduler(sched);
  SYSCALL(sem_init(&idleSem, 0, 0));   // create idle-loop wake-up semaphore
  pthread_attr_t attr;                 // create pthread running idleLoop
  SYSCALL(pthread_attr_init(&attr));
  SYSCALL(pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED));
#if __linux__ // FreeBSD jemalloc segfaults when trying to use minimum stack
  SYSCALL(pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN));
#endif
  SYSCALL(pthread_create(&sysThread, &attr, idleLoopSetupPthread, this));
  SYSCALL(pthread_attr_destroy(&attr));
}

SystemProcessor::SystemProcessor(Scheduler& sched, funcvoid1_t func, ptr_t arg) {
  setScheduler(sched);
  SYSCALL(sem_init(&idleSem, 0, 0));   // create idle-loop wake-up semaphore
  sysThread = pthread_self();
  Context::currProc = this;
  // idle loop takes over pthread stack - create fibre without stack
  setupIdle(new Fibre(_friend<SystemProcessor>()));
  Context::currStack = idleStack;
  // tmp fibre will run first on this new SP
  Fibre* tmpFibre = new Fibre(_friend<SystemProcessor>(), defaultStackSize);
  tmpFibre->setAffinity()->setPriority(topPriority);
  tmpFibre->run(func, arg);
  ((Fibre*)idleStack)->runDirect(idleLoopSetupFibre, this, _friend<SystemProcessor>());
}

SystemProcessor::SystemProcessor(Scheduler& sched, _friend<_Bootstrapper>) {
  setScheduler(sched);
  SYSCALL(sem_init(&idleSem, 0, 0));   // create idle-loop wake-up semaphore
  sysThread = pthread_self();          // main pthread runs main routine
}

Fibre* SystemProcessor::init(_friend<_Bootstrapper>) {
  Context::currProc = this;
  // create proper idle loop fibre
  setupIdle(new Fibre(_friend<SystemProcessor>(), defaultStackSize));
  idleStack->setup((ptr_t)idleLoopSetupFibre, this);
  // main fibre takes over pthread stack - create fibre without stack
  Fibre* mainFibre = new Fibre(_friend<SystemProcessor>());
  Context::currStack = mainFibre;
  return mainFibre;
}

SystemProcessor::~SystemProcessor() {
  // leave LB - no more regular work added
  currScheduler->remove(*this);
  // make sure current fibre is off this scheduler
  if (this == &CurrProcessor()) Fibre::migrateSelf(*currScheduler);
  GENASSERT(this != &CurrProcessor());
  // pthread's exit removes thread_local Context when idle loop terminates
  idleTerminate = true;             // terminate idle loop
  wakeUp();                         // wake up idle loop (just in case)
  delete (Fibre*)idleStack;         // wait for idle loop to finish
  SYSCALL(sem_destroy(&idleSem));   // destroy idle-loop wake-up semaphore
}

inline void SystemProcessor::idleLoop() {
  for (;;) {
    if (!busyLoop()) break;
    // report idle and wait, if enough spins have passed
    if (reinterpret_cast<FibreScheduler*>(currScheduler)->addIdleProcessor(*this)) {
      stats->idle.count();
      SYSCALL(sem_wait(&idleSem));
      currScheduler->removeIdleProcessor(*this);
    }
  }
}

void SystemProcessor::idleLoopSetupFibre(void* sp) {
  reinterpret_cast<SystemProcessor*>(sp)->idleLoop();
}

void* SystemProcessor::idleLoopSetupPthread(void* sp) {
  SystemProcessor* This = reinterpret_cast<SystemProcessor*>(sp);
  Context::currProc = This;
  // idle loop takes over pthread stack - create fibre without stack
  This->setupIdle(new Fibre(_friend<SystemProcessor>()));
  Context::currStack = This->idleStack;
  ((Fibre*)This->idleStack)->runDirect(idleLoopSetupFibre, This, _friend<SystemProcessor>());
  return nullptr;
}
