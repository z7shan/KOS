/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _EventEngine_h_
#define _EventEngine_h_ 1

#include "libfibre/SystemProcessor.h"

#include <vector>
#include <unistd.h>       // close
#include <sys/resource.h> // getrlimit
#include <sys/types.h>
#include <sys/socket.h>

#if __linux__
#include <sys/timerfd.h>
#endif

class BaseProcessor;

class EventEngine {
  // A vector for FDs works well here in principle, because POSIX guarantees lowest-numbered FDs:
  // http://pubs.opengroup.org/onlinepubs/9699919799/functions/V2_chap02.html#tag_15_14
  // A fixed-size array based on 'getrlimit' is somewhat brute-force, but simple and fast.
#ifdef TESTING_POLLSYNC_OPTIMISTIC
  struct SyncIO {
    BaseSemaphore<BinaryLock<>,true> sem;
    mword           cnt;
    SyncIO() : cnt(0) {}
    void reset() { sem.reset(); cnt = 0; }
  };
#else
  struct SyncIO {
    BaseSemaphore<BinaryLock<>,true> sem;
    SyncIO() : sem(1) {}
    void reset() { sem.reset(1); }
  };
#endif

  struct SyncRW {
    SyncIO RD;
    SyncIO WR;
  } *fdSyncVector;

  // file operations are not considered blocking in terms of select/poll/epoll
  // therefore, all file operations are executed on dedicated Scheduler/SP(s)
  FibreScheduler  ioScheduler;
  SystemProcessor ioSP;

  int           timerFD;
  Fibre*        timerFibre;
  volatile bool timerTerminate;

  inline void   timerLoop();
  static void   timerLoopSetup(void*);

public:
#if TESTING_MASTER_POLLER || TESTING_POLLER_FIBRES
  PollerThread  masterPoller;                // runs on ioScheduler, but with idleThreshold = 0
#endif

  EventEngine() : fdSyncVector(0), ioScheduler(0), ioSP(ioScheduler), timerTerminate(false) {
    struct rlimit rl;
    SYSCALL(getrlimit(RLIMIT_NOFILE, &rl));  // get hard limit for file descriptor count
    unsigned long fdcount = rl.rlim_max;
#if __FreeBSD__
    timerFD = fdcount;
    fdcount += 1;
#endif
    fdSyncVector = new SyncRW[fdcount];      // create vector of R and W sync points
  }

  ~EventEngine() {
    timerTerminate = true;                   // set timer loop termination flag
    notifyTimeout({0,1});                    // wake up timer loop
    delete timerFibre;
#if __linux__
    close(timerFD);
#endif
    delete[] fdSyncVector;
  }

  void startTimerHandling() {
#if __linux__
    timerFD = timerfd_create(CLOCK_REALTIME, TFD_NONBLOCK | TFD_CLOEXEC);
#if TESTING_MASTER_POLLER || TESTING_POLLER_FIBRES
    masterPoller.registerFD(timerFD);
#else
    CurrPoller().registerFD(timerFD);
#endif
#endif
    timerFibre = (new Fibre)->setAffinity()->setPriority(topPriority)->run(timerLoopSetup, this);
  }

  void notifyTimeout(const Time& timeout) {
#if __FreeBSD__
#if TESTING_MASTER_POLLER || TESTING_POLLER_FIBRES
    masterPoller.setTimer(timerFD, timeout);
#else
    CurrPoller().setTimer(timerFD, timeout);
#endif
#else // __linux__ below
    itimerspec tval = { {0,0}, timeout };
    SYSCALL(timerfd_settime(timerFD, TFD_TIMER_ABSTIME, &tval, nullptr));
#endif // __FreeeBSD__ vs. __linux__
  }

  void registerSyncFD(int fd) {
    CurrPoller().registerFD(fd);
  }
  void deregisterSyncFD(int fd) {
    fdSyncVector[fd].RD.reset();
    fdSyncVector[fd].WR.reset();
  }
  void suspendFD(int fd) {
    fdSyncVector[fd].RD.sem.P_fake();
    fdSyncVector[fd].WR.sem.P_fake();
  }
  void resumeFD(int fd) {
    fdSyncVector[fd].RD.sem.V();
    fdSyncVector[fd].WR.sem.V();
  }

  template<bool Input>
  void block(int fd) {
    SyncIO& sync = Input ? fdSyncVector[fd].RD : fdSyncVector[fd].WR;
    sync.sem.P();
  }

  template<bool Input>
  void unblock(int fd, ProcessorResumeSet& procSet, _friend<BasePoller>) {
    SyncIO& sync = Input ? fdSyncVector[fd].RD : fdSyncVector[fd].WR;
#if TESTING_BULK_RESUME
    sync.sem.V_bulk(procSet);
#else
    sync.sem.V();
#endif
  }

#if TESTING_POLLER_FIBRES
  void unblockPoller(int fd, _friend<Poller>) {
    fdSyncVector[fd].RD.sem.V();
  }
#endif

  template<typename T, class... Args>
  T directIO(T (*iofunc)(Args...), Args... a) {
    VirtualProcessor& proc = Fibre::migrateSelf(ioScheduler, _friend<EventEngine>());
    int result = iofunc(a...);
    Fibre::migrateSelf(proc, _friend<EventEngine>());
    return result;
  }

#ifdef TESTING_POLLSYNC_OPTIMISTIC
  template<bool Input, bool Yield, typename T, class... Args>
  T syncIO( T (*iofunc)(int, Args...), int fd, Args... a) {
    SyncIO& sync = Input ? fdSyncVector[fd].RD : fdSyncVector[fd].WR;
    if (Yield) Fibre::yield();
    __atomic_add_fetch(&sync.cnt, 1, __ATOMIC_RELAXED);
    for (;;) {
      T ret = iofunc(fd, a...);
      if (ret >= 0) {                // sucess -> notify next fibre waiting
        if (__atomic_sub_fetch(&sync.cnt, 1, __ATOMIC_RELAXED)) sync.sem.V();
        return ret;
      } else if (errno != EAGAIN) {  // failure
        __atomic_sub_fetch(&sync.cnt, 1, __ATOMIC_RELAXED);
        return ret;
      }
      sync.sem.P();                  // block
    }
  }
#else
  template<bool Input, bool Yield, typename T, class... Args>
  T syncIO( T (*iofunc)(int, Args...), int fd, Args... a) {
    SyncIO& sync = Input ? fdSyncVector[fd].RD : fdSyncVector[fd].WR;
    T ret;
    if (!Input) {                    // assume output typically does not block
      if (Yield) Fibre::yield();
      ret = iofunc(fd, a...);
      if (ret >= 0 || errno != EAGAIN) return ret;
    }
    do {
      sync.sem.P(Yield && Input);
      ret = iofunc(fd, a...);
    } while (ret < 0 && errno == EAGAIN);
    sync.sem.V();
    return ret;
  }
#endif
};

extern EventEngine* _lfEventEngine;

// input: yield before network read
template<typename T, class... Args>
T lfInput( T (*readfunc)(int, Args...), int fd, Args... a) {
  return _lfEventEngine->syncIO<true,true>(readfunc, fd, a...);
}

// output: no yield before write
template<typename T, class... Args>
T lfOutput( T (*writefunc)(int, Args...), int fd, Args... a) {
  return _lfEventEngine->syncIO<false,false>(writefunc, fd, a...);
}

// direct I/O
template<typename T, class... Args>
T lfDirectIO( T (*iofunc)(int, Args...), int fd, Args... a) {
  return _lfEventEngine->directIO(iofunc, fd, a...);
}

// socket: register SOCK_DGRAM fd for I/O synchronization; SOCK_STREAM later (cf. listen, connect)
static inline int lfSocket(int domain, int type, int protocol) {
  int fd = socket(domain, type | SOCK_NONBLOCK, protocol);
  if (fd >= 0 && type != SOCK_STREAM) _lfEventEngine->registerSyncFD(fd);
  return fd;
}

// POSIX says that bind might fail with EINPROGRESS, but not on Linux...
static inline int lfBind(int fd, const sockaddr *addr, socklen_t addrlen) {
  int ret = bind(fd, addr, addrlen);
  if (ret < 0 && errno == EINPROGRESS) {
    _lfEventEngine->block<true>(fd);
    socklen_t sz = sizeof(ret);
    SYSCALL(getsockopt(fd, SOL_SOCKET, SO_ERROR, &ret, &sz));
  }
  return ret;
}

// listen: can register SOCK_STREAM fd only after 'listen' system call on FreeBSD
static inline int lfListen(int fd, int backlog) {
  int ret = listen(fd, backlog);
  _lfEventEngine->registerSyncFD(fd);
  return ret;
}

// accept: register new file descriptor for I/O events, no yield before accept
static inline int lfAccept(int fd, sockaddr *addr, socklen_t *addrlen, int flags = 0) {
  int ret = _lfEventEngine->syncIO<true,false>(accept4, fd, addr, addrlen, flags | SOCK_NONBLOCK);
  if (ret >= 0) _lfEventEngine->registerSyncFD(ret);
  return ret;
}

// see man 3 connect for EINPROGRESS; register SOCK_STREAM fd now (FreeBSD, cf. listen)
static inline int lfConnect(int fd, const sockaddr *addr, socklen_t addrlen) {
  _lfEventEngine->registerSyncFD(fd);
  int ret = connect(fd, addr, addrlen);
  if (ret < 0 && errno == EINPROGRESS) {
    _lfEventEngine->block<false>(fd);
    socklen_t sz = sizeof(ret);
    SYSCALL(getsockopt(fd, SOL_SOCKET, SO_ERROR, &ret, &sz));
  }
  return ret;
}

// shutdown: deregister file descriptor -> reset RD/WR synchronization
static inline int lfShutdown(int fd, int how) {
  _lfEventEngine->deregisterSyncFD(fd);
  return shutdown(fd, how);
}

// close: deregister file descriptor -> reset RD/WR synchronization
static inline int lfClose(int fd) {
  _lfEventEngine->deregisterSyncFD(fd); // fd might be reused after close
  return close(fd);
}

#endif /* _EventEngine_h_ */
