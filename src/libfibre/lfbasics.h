/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _lfbasics_h_
#define _lfbasics_h_ 1

#include "generic/basics.h"

#include <atomic>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/syscall.h>

// **** bootstrap object needs to come first

static class _Bootstrapper {
  static std::atomic<int> counter;
public:
  _Bootstrapper();
  ~_Bootstrapper();
} _lfBootstrap;

// **** checks and balances, using gcc/clang's 'expression statement' extension


#if TESTING_ENABLE_ASSERTIONS
static void _SYSCALLabort() { exit(1); }
extern void _SYSCALLabortLock();
extern void _SYSCALLabortUnlock();
#define SYSCALL_EQ(call,expected) ({\
  int ret ## __COUNTER__ = call;\
  if (ret ## __COUNTER__ != expected) {\
    _SYSCALLabortLock();\
    printf("FAILED SYSCALL: %s -> %d (expected == %lli), errno: %d\nat: %s:%d\n", #call, ret ## __COUNTER__, (long long)expected, errno, __FILE__, __LINE__);\
    _SYSCALLabortUnlock();\
    _SYSCALLabort();\
  }\
  ret ## __COUNTER__; })
#define SYSCALL_NE(call,expected) ({\
  int ret ## __COUNTER__ = call;\
  if (ret ## __COUNTER__ == expected) {\
    _SYSCALLabortLock();\
    printf("FAILED SYSCALL: %s -> %d (expected != %lli), errno: %d\nat: %s:%d\n", #call, ret ## __COUNTER__, (long long)expected, errno, __FILE__, __LINE__);\
    _SYSCALLabortUnlock();\
    _SYSCALLabort();\
  }\
  ret; })
#define SYSCALL_GE(call,expected) ({\
  int ret ## __COUNTER__ = call;\
  if (ret ## __COUNTER__ < expected) {\
    _SYSCALLabortLock();\
    printf("FAILED SYSCALL: %s -> %d (expected >= %lli), errno: %d\nat: %s:%d\n", #call, ret ## __COUNTER__, (long long)expected, errno, __FILE__, __LINE__);\
    _SYSCALLabortUnlock();\
    _SYSCALLabort();\
  }\
  ret ## __COUNTER__; })
#else
#define SYSCALL_EQ(call,expected) call
#define SYSCALL_NE(call,expected) call
#define SYSCALL_GE(call,expected) call
#endif

#define SYSCALL(call)   SYSCALL_EQ(call,0)
#define SYSCALLIO(call) SYSCALL_GE(call,0)
#define uabort(msg) { std::cerr << msg << std::endl; abort(); }

// **** locking

class SystemLock {
  pthread_mutex_t mutex;
  friend class SystemCondition;
public:
  SystemLock() {
    pthread_mutexattr_t attr;
    SYSCALL(pthread_mutexattr_init(&attr));
#if TESTING_MUTEX_ERRORCHECKING
    SYSCALL(pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK));
#endif
    SYSCALL(pthread_mutex_init(&mutex, &attr));
    SYSCALL(pthread_mutexattr_destroy(&attr));
  }
  ~SystemLock() {
    SYSCALL(pthread_mutex_destroy(&mutex));
  }
  bool tryAcquire() {
    return pthread_mutex_trylock(&mutex) == 0;
  }
  void acquire() {
#if TESTING_PTHREAD_SPINLOCK
    while (pthread_mutex_trylock(&mutex) != 0) Pause();
#else
    SYSCALL(pthread_mutex_lock(&mutex));
#endif
  }
  bool acquire(const Time& timeout) {
    return pthread_mutex_timedlock(&mutex, &timeout) == 0;
  }
  void release() {
    SYSCALL(pthread_mutex_unlock(&mutex));
  }
  bool test() {
    if (!tryAcquire()) return true;
    release();
    return false;
  }
};

class SystemCondition {
  pthread_cond_t cond;
public:
  SystemCondition() {
    SYSCALL(pthread_cond_init(&cond, nullptr));
  }
  ~SystemCondition() {
    SYSCALL(pthread_cond_destroy(&cond));
  }
  void clear(SystemLock& lock) {
    SYSCALL(pthread_cond_broadcast(&cond));
    lock.release();
  }
  bool block(SystemLock& lock, bool wait, const Time& timeout = Time::zero()) {
    bool ret = false;
    if (wait) {
      if (timeout == Time::zero()) {
        SYSCALL(pthread_cond_wait(&cond, &lock.mutex));
        ret = true;
      } else {
        ret = pthread_cond_timedwait(&cond, &lock.mutex, &timeout) == 0;
      }
    }
    lock.release();
    return ret;
  }
  void wait(SystemLock& lock, const Time& timeout = Time::zero()) {
    block(lock, true, timeout);
  }
  void signal() {
    SYSCALL(pthread_cond_signal(&cond));
  }
};

class SystemLockRW {
  pthread_rwlock_t rwlock;
public:
  SystemLockRW() {
    SYSCALL(pthread_rwlock_init(&rwlock, nullptr));
  }
  ~SystemLockRW() {
    SYSCALL(pthread_rwlock_destroy(&rwlock));
  }
  void acquireRead() {
    SYSCALL(pthread_rwlock_rdlock(&rwlock));
  }
  bool acquireRead(const Time& timeout) {
    return pthread_rwlock_timedrdlock(&rwlock, &timeout) == 0;
  }
  void acquireWrite() {
    SYSCALL(pthread_rwlock_wrlock(&rwlock));
  }
  bool acquireWrite(const Time& timeout) {
    return pthread_rwlock_timedwrlock(&rwlock, &timeout) == 0;
  }
  void release() {
    SYSCALL(pthread_rwlock_unlock(&rwlock));
  }
  bool tryAcquireRead() {
    return pthread_rwlock_tryrdlock(&rwlock) == 0;
  }
  bool tryAcquireWrite() {
    return pthread_rwlock_trywrlock(&rwlock) == 0;
  }
};

class SystemSemaphore {
  sem_t sem;
public:
  SystemSemaphore(size_t c = 0) {
    SYSCALL(sem_init(&sem, 0, c));
  }
  ~SystemSemaphore() {
    SYSCALL(sem_destroy(&sem));
  }
  bool empty() {
    int val;
    SYSCALL(sem_getvalue(&sem, &val));
    return val >= 0;
  }
  bool open() {
    int val;
    SYSCALL(sem_getvalue(&sem, &val));
    return val > 0;
  }
  bool tryP() {
    return sem_trywait(&sem) == 0;
  }
  bool P() {
    SYSCALL(sem_wait(&sem));
    return true;
  }
  bool P(const Time& timeout) {
    return sem_timedwait(&sem, &timeout) == 0;
  }
  void V() {
    SYSCALL(sem_post(&sem));
  }
};

// **** debug output

extern SystemLock* _lfDebugOutputLock;

inline void dprint() {}

template<typename T, typename... Args>
inline void dprint(T x, const Args&... a) {
  std::cerr << x;
  dprint(a...);
}

template<typename... Args>
inline void dprintl(const Args&... a) {
#if 0
  ScopedLock<SystemLock> al(*_lfDebugOutputLock);
  dprint(syscall(__NR_gettid), ' ', a..., '\n');
#endif
}

// **** system processor (here pthread) context

struct Runtime;
class SystemProcessor;
class StackContext;

// it seems noinline is needed for TLS and then volatile is free anyway...
// http://stackoverflow.com/questions/25673787/making-thread-local-variables-fully-volatile
// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=66631
class Context {
protected:
  static thread_local SystemProcessor* volatile currProc;
  static thread_local StackContext*    volatile currStack;
public:
  static void setCurrStack(StackContext& s, _friend<Runtime>) __no_inline;
  static SystemProcessor* self()   __no_inline;
  static StackContext* CurrStack() __no_inline;
};

static inline StackContext* CurrStack() {
  StackContext* s = Context::CurrStack();
  GENASSERT(s);
  return s;
}

// **** globally visible routines

extern void _lfNotifyTimeout(const Time& timeout);

// **** global constants

static const size_t defaultStackSize = 16 * pagesize<1>();
static const size_t stackProtection = pagesize<1>();

#endif /* _lfbasics_h_ */
