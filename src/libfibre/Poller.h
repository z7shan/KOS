/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Poller_h_
#define _Poller_h_ 1

#if __FreeBSD__
#include <sys/event.h>
#else // __linux__ below
#include <sys/eventfd.h>
#include <sys/epoll.h>
#endif

class Fibre;
class PollerScheduler;

class BasePoller {
protected:
#if TESTING_EXPONENTIAL_EPOLL
  static const int maxPoll   = 8192;
  static const int firstPoll = 16;
#else
  static const int maxPoll   = 256;
  static const int firstPoll = 256;
#endif
  int           pollFD;
#if __FreeBSD__
  struct kevent events[maxPoll];
#else // __linux__ below
  epoll_event   events[maxPoll];
#endif
  volatile bool pollTerminate;

  inline void   poll(int evcnt);

  PollerStats *stats;

public:
  BasePoller() : pollTerminate(false) {
    stats = new PollerStats(this);
#if __FreeBSD__
    pollFD = SYSCALLIO(kqueue());
#else // __linux__ below
    pollFD = SYSCALLIO(epoll_create1(EPOLL_CLOEXEC));
#endif
  }
  ~BasePoller() {
    close(pollFD);
  }

  template<bool ReadWrite = true>
  void registerFD(int fd) {
#if __FreeBSD__
    struct kevent ev[2];
    EV_SET(&ev[0], fd, EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, 0);
    if (ReadWrite) EV_SET(&ev[1], fd, EVFILT_WRITE, EV_ADD | EV_CLEAR, 0, 0, 0);
    SYSCALL(kevent(pollFD, ev, ReadWrite ? 2 : 1, nullptr, 0, nullptr));
#else // __linux__ below
    epoll_event ev;
    ev.events = EPOLLIN | EPOLLRDHUP | EPOLLPRI | EPOLLERR | EPOLLHUP | EPOLLET;
    if (ReadWrite) ev.events |= EPOLLOUT;
    ev.data.fd = fd;
    SYSCALL(epoll_ctl(pollFD, EPOLL_CTL_ADD, fd, &ev));
#endif
  }

#if __FreeBSD__
  void setTimer(uintptr_t id, const Time& timeout) {
    Time ct;
    SYSCALL(clock_gettime(CLOCK_REALTIME, &ct));
    if (ct > timeout) ct = Time::zero();
    else ct = timeout - ct;
    struct kevent ev;
    EV_SET(&ev, id, EVFILT_TIMER, EV_ADD | EV_ONESHOT, NOTE_USECONDS, ct.toUS(), 0);
    SYSCALL(kevent(pollFD, &ev, 1, nullptr, 0, nullptr));
  }
#endif

};

class PollerThread : public BasePoller {
  pthread_t pollThread;
  static void* pollLoopSetup(void*);
protected:
  template<typename T> inline void pollLoop();
protected:
  PollerThread(void *(*loopSetup)(void*)) {
    SYSCALL(pthread_create(&pollThread, nullptr, pollLoopSetup, this));
  }
public:
  PollerThread() : PollerThread(pollLoopSetup) {}
  ~PollerThread() { if (!pollTerminate) stop(); }
  pthread_t getSysID() { return pollThread; }
  bool wait(_friend<PollerThread>) { return false; }
  void stop() {                    // use self-pipe trick to terminate poll loop
    pollTerminate = true;
#if __FreeBSD__
    struct kevent ev;
    EV_SET(&ev, 0, EVFILT_USER, EV_ADD | EV_ONESHOT, 0, 0, 0);
    SYSCALL(kevent(pollFD, &ev, 1, nullptr, 0, nullptr));
    EV_SET(&ev, 0, EVFILT_USER, EV_ENABLE, NOTE_TRIGGER, 0, 0);
    SYSCALL(kevent(pollFD, &ev, 1, nullptr, 0, nullptr));
    SYSCALL(pthread_join(pollThread, nullptr));
#else // __linux__ below
    int efd = SYSCALLIO(eventfd(0, EFD_CLOEXEC | EFD_NONBLOCK));
    registerFD(efd);
    uint64_t val = 1;
    val = SYSCALL_EQ(write(efd, &val, sizeof(val)), sizeof(val));
    SYSCALL(pthread_join(pollThread, nullptr));
    SYSCALL(close(efd));
#endif
  }
};

#if TESTING_POLLER_FIBRES
class Poller : public BasePoller {
  Fibre*      pollFibre;
  inline void pollLoop();
  static void pollLoopSetup(void*);
public:
  Poller(PollerScheduler&);
  ~Poller() { if (!pollTerminate) stop(); }
  void stop();
  bool stopped() { return pollTerminate; }
};
#else
class Poller : public PollerThread {
  PollerScheduler& sched;
  static void* pollLoopSetup(void*);
public:
  Poller(PollerScheduler& sched) : PollerThread(pollLoopSetup), sched(sched) {}
  inline bool wait(_friend<PollerThread>);
};
#endif

#endif /* _Poller_h_ */
